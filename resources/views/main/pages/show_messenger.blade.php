@extends('main.layouts.master')
@section('content')
    {{--            <div class="row">--}}
    {{--                <div class="col-sm-12">--}}
    {{--                    <div class="card">--}}
    {{--                        <div class="card-header">--}}
    {{--                            <h5>Large Table</h5>--}}
    {{--                            <span>Example of large table, Add <code>.table-lg</code>class to the <code>.table</code>to create a table with large spacing. larger table all rows have <code>0.9rem</code> height.</span>--}}
    {{--                        </div>--}}
    {{--                        <div class="table-responsive">--}}
    {{--                            <table class="table table-lg">--}}
    {{--                                <thead>--}}
    {{--                                <tr>--}}
    {{--                                    <th scope="col">№</th>--}}
    {{--                                    <th scope="col">Yuborgan shaxs</th>--}}
    {{--                                    <th scope="col">Yuborilgan vaqti</th>--}}
    {{--                                    <th scope="col">doc fayl</th>--}}
    {{--                                    <th scope="col">pdf fayl</th>--}}
    {{--                                    <th scope="col">taqriz fayl</th>--}}
    {{--                                </tr>--}}
    {{--                                </thead>--}}
    {{--                                <tbody>--}}
    {{--                                @foreach($accepts as $key => $accept)--}}
    {{--                                    @if($date[$key]->start_date <= $accept->created_at && $date[$key]->end_date >= $accept->created_at)--}}
    {{--                                    <tr>--}}
    {{--                                        <th scope="row">{{ $loop->index + 1 }}</th>--}}
    {{--                                        <td>{{ $accept->users->name }}</td>--}}
    {{--                                        <td>{{ $accept->created_at }}</td>--}}
    {{--                                        <td>--}}
    {{--                                            <a href="{{asset('files/'.$accept->docFormat)}}" class="btn btn-primary" type="button" data-bs-toggle="tooltip" title="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>--}}
    {{--                                        </td>--}}
    {{--                                        <td>--}}
    {{--                                            <a href="{{asset('files/'.$accept->pdfFormat)}}" class="btn btn-secondary" type="button" data-bs-toggle="tooltip" title="btn btn-secondary"><i class="fa fa-download"></i>&nbsp;&nbsp;PDF</a>--}}

    {{--                                        </td>--}}
    {{--                                        <td>--}}
    {{--                                            <a href="{{asset('files/'.$accept->taqriz)}}" class="btn btn-info" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>--}}
    {{--                                        </td>--}}
    {{--                                    </tr>--}}
    {{--                                    @endif--}}
    {{--                                @endforeach--}}
    {{--                                </tbody>--}}
    {{--                            </table>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    @if($accepts)
    <div class="container-fluid">
        <div class="user-profile">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @foreach($accepts as $accept)
                            <div class="profile-img-style">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="media"><img class="img-thumbnail rounded-circle me-3"
                                                                src="{{ asset('users/assets/images/107224_document_504x512.png') }}"
                                                                alt="Generic placeholder image">
                                            <div class="media-body align-self-center">
                                                <h5 class="mt-0 user-name" style="font-weight: bold; text-transform: capitalize">{{ $accept->articleTitle }}</h5>
                                                <span class="mt-0" style="font-style: italic">Mualliflar: {{ $accept->author ? $accept->author : "Yo'q" }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 align-self-center">
                                        <div class="float-sm-end"><small> <a target="_blank"
                                                                             href="{{asset('files/'.$accept->file)}}"
                                                                             class="btn btn-secondary" type="button"
                                                                             data-bs-toggle="tooltip"><i
                                                        class="fa fa-download"></i>&nbsp;&nbsp;PDF</a></small></div>
                                    </div>
                                </div>
                                <hr>
                                <p>{{\Illuminate\Support\Str::limit($accept->articleAnotation, 800, $end='.......')}}</p>
                                <div class="like-comment mt-4">
                                    <ul class="list-inline">
                                        <li class="list-inline-item border-right pe-3">
                                            <label class="m-0"><a href="#" data-bs-original-title="" title=""><i
                                                        class="fa fa-heart"></i></a>&nbsp;&nbsp;Like</label><span
                                                class="ms-2 counter">2659</span>
                                        </li>
                                        <li class="list-inline-item ms-2">
                                            <label class="m-0"><a href="#" data-bs-original-title="" title=""><i
                                                        class="fa fa-download"></i></a>&nbsp;&nbsp;Yuklab olindi</label><span
                                                class="ms-2 counter">569</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                        <div style="display: flex;
    padding: 10px;
    justify-content: right;">{{ ($accepts->count() > 0) ? $accepts->links() : '' }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection
