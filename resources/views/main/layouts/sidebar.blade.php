{{--<div class="sidebar-wrapper">--}}
{{--    <div>--}}
{{--        <div class="logo-wrapper"><a href=""><img class="img-fluid for-light"--}}
{{--                                                  src="{{asset('users/assets/images/logo/logo.png')}}" alt=""><img--}}
{{--                    class="img-fluid for-dark" src="{{asset('users/assets/images/logo/logo_dark.png')}}" alt=""></a>--}}
{{--            <div class="back-btn"><i class="fa fa-angle-left"></i></div>--}}
{{--            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>--}}
{{--        </div>--}}
{{--        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid"--}}
{{--                                                                 src="{{asset('users/assets/images/logo/logo-icon.png')}}"--}}
{{--                                                                 alt=""></a></div>--}}
{{--        <nav class="sidebar-main">--}}
{{--            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>--}}
{{--            <div id="sidebar-menu">--}}
{{--                <u
l class="sidebar-links" id="simple-bar">--}}
{{--                    <li class="back-btn"><a href="index.html"><img class="img-fluid"--}}
{{--                                                                   src="{{asset('users/assets/images/logo/logo-icon.png')}}"--}}
{{--                                                                   alt=""></a>--}}
{{--                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2"--}}
{{--                                                                              aria-hidden="true"></i></div>--}}
{{--                    </li>--}}
{{--                    <li class="sidebar-main-title">--}}
{{--                        <div>--}}
{{--                            <h6 class="">Mjournal</h6>--}}
{{--                            <p class="">Buxoro Davlat Universiteti</p>--}}
{{--                        </div>--}}
{{--                    </li>--}}

{{--                    @if($article)--}}
{{--                        @foreach($maqolaaas as $maqola)--}}
{{--                            @if($maqola->year <= date('Y'))--}}
{{--                                <li class="sidebar-list">--}}

{{--                                    <span>{{ $maqola->year }}</span>--}}
{{--                                        <ul class="sidebar-submenu" style="display: none;">--}}
{{--                                            @foreach($maqolas as $maq)--}}
{{--                                                @if($maq->show_date <= date('Y-m-d', time()))--}}
{{--                                                    @if($maqola->year == $maq->year)--}}
{{--                                                    <li>--}}
{{--                                                        <a href="{{ route('show_messenger', ['publication'=>$maq->publication,'year'=>$maq->year]) }}">{{ 'Maqolaning ' . $maq->publication . ' - soni' }}</a>--}}
{{--                                                    </li>--}}
{{--                                                        @endif--}}
{{--                                                @endif--}}
{{--                                            @endforeach--}}
{{--                                        </ul>--}}
{{--                                </li>--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
{{--                    @endif--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>--}}
{{--        </nav>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="col-lg-4">
    <div class="sidebar">
        <h3 class="sidebar-title">Last publications</h3>
        <div class="accordion accordion-flush" id="faqlist1">
            @if($article)
                @foreach($maqolaaas as $maqola)
                    @if($maqola->year <= date('Y'))
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
                                    {{$maqola->year}}
                                </button>
                            </h2>
                            <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist1">
                                @foreach($maqolas as $maq)
                                    @if($maq->show_date <= date('Y-m-d', time()))
                                        @if($maqola->year == $maq->year)
                                                <a href="{{ route('show_messenger', ['publication'=>$maq->publication,'year'=>$maq->year]) }}">{{ 'Maqolaning ' . $maq->publication . ' - soni' }}</a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
        <h3 class="sidebar-title">Journals</h3>
        <div class="sidebar-item tags">
            <ul>
                @foreach($journals as $journal)
                    <li><a href="#">{{$journal->sciensName}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>

