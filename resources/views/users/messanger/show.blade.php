@extends('users.layouts.master')
@section('title','Kelgan izohlar')
@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-sm-10 m-auto mt-5">
        <div class="card chat-default">
            <div class="card-header card-no-border">
                <div class="media media-dashboard">
                    <div class="media-body">
                        <h5 class="mb-0">Nashriyot administratoridan yuborilgan xabarlar</h5>
                    </div>
                    <div class="icon-box"><i class="fa fa-ellipsis-h"></i></div>
                </div>
            </div>
            <div class="card-body chat-box">
                <div class="chat">
                    @foreach($chats as $chat)
                    <div class="media left-side-chat">
                        <div class="media-body d-flex">
                            <div class="img-profile"> <img class="img-fluid" src="{{asset('users/assets/images/user.jpg')}}" alt="Profile"></div>
                            <div class="main-chat w-75">
                                <div class="message-main"><span class="mb-0">{{$chat->messanger}}</span></div>

                            </div>
                        </div>
                        <p class="f-w-400">{{$chat->created_at}}</p>
                    </div>
                    <br>

{{--                    <div class="media left-side-chat">--}}
{{--                        <div class="media-body d-flex">--}}
{{--                            <div class="img-profile"> <img class="img-fluid" src="{{asset('users/assets/images/user.jpg')}}" alt="Profile"></div>--}}
{{--                            <div class="main-chat">--}}
{{--                                <div class="sub-message message-main mt-0"><span>It's argently</span></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <p class="f-w-400">7:28 PM</p>--}}
{{--                    </div>--}}
                    @endforeach
{{--                        <div class="media left-side-chat">--}}
{{--                            <div class="media-body d-flex">--}}
{{--                                <div class="img-profile"> <img class="img-fluid" src="{{asset('users/assets/images/user.jpg')}}" alt="Profile"></div>--}}
{{--                                <div class="main-chat w-75">--}}
{{--                                    <div style="border-radius: 5px; color: white; padding: 5px" class="badge-success">Объединив открытые технологии RedОбъединив открытые технологии RedОбъединив открытые технологии Red Hat с ведущими компьютерными архитектурами и программными инструментами Intel, компании ускоряют развитие экосистемы АСУ</div>--}}
{{--                                    <div class="message-main"><span class="mb-0"> Edge-вычисления в малом форм-факторе. Комбинируя платформу приложений Red Hat OpenShift с широким спектром качественных, надежных и простых в использовании малогабаритных устройств семейства Intel NUC , можно совершить качественный скачок в масштабируемости решений класса «Индустрия 4.0»</span></div>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <p class="f-w-400"></p>--}}
{{--                        </div>--}}
                    <div class="media right-side-chat">
                        <div class="media-body text-end">
                            <div class="message-main pull-right"><span class="loader-span mb-0 text-start" id="wave"><span class="dot"></span><span class="dot"></span><span class="dot"></span></span></div>
                        </div>
                    </div>
{{--                    <div class="input-group">--}}
{{--                        <input class="form-control" id="mail" type="text" placeholder="Type Your Message..." name="text">--}}
{{--                        <div class="send-msg"><i data-feather="send"></i></div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection
