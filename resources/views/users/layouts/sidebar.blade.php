<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="{{route('user.dashboard')}}"><img class="img-fluid for-light" src="{{asset('users/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{asset('users/assets/images/logo/logo_dark.png')}}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('users/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{asset('users/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6 class="">Mjournal</h6>
                            <p class="">Buxoro Davlat Universiteti</p>
                        </div>
                    </li>

                    <li class="sidebar-list"><label class="badge badge-danger">Yangi</label><a class="sidebar-link sidebar-title link-nav" href="{{route('articles.create')}}"><i style="color: {{request()->is('user/articles/create') ? '#7366ff' : ''}}" data-feather="send"> </i><span style="color: {{request()->is('user/articles/create') ? '#7366ff' : ''}}">Maqola yuborish</span></a></li>

                    <li class="sidebar-list"><a class="sidebar-link sidebar-title link-nav" href="{{route('articles.index')}}"><i style="color: {{request()->is('user/articles') ? '#7366ff' : ''}}" data-feather="archive"> </i><span style="color: {{request()->is('user/articles') ? '#7366ff' : ''}}">Yuborilgan maqolalar</span></a></li>
                    <li class="sidebar-list"><label class="badge badge-success">Hoziroq</label><a class="sidebar-link sidebar-title link-nav" href="{{ route('user.subscription') }}"><i style="color: {{request()->is('user/subscription') ? '#7366ff' : ''}}" data-feather="check-square"> </i><span style="color: {{request()->is('user/subscription') ? '#7366ff' : ''}}">Obuna bo'lish</span></a></li>
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
