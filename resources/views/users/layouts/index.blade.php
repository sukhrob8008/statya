

@extends('users.layouts.master')
@section('title','Shaxsiy profil')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-6">
                        <!-- <h3>Sample Page</h3> -->
                    </div>
                    <div class="col-6">
                        <ol class="breadcrumb">
{{--                            <li class="breadcrumb-item"><a href="index.html">                                       <i data-feather="home"></i></a></li> -->--}}
{{--                             <li class="breadcrumb-item">Pages</li>--}}
{{--                             <li class="breadcrumb-item active">Sample Page</li>--}}
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                    @foreach($rules as $rule)
                        <div class="card-header">
                            <label class="badge badge-danger f-22">Umumiy qoidalar</label><h3>{{$rule->title}}</h3>
                        </div>
                        <div class="card-body" style="text-align: justify">
                            <p>{{$rule->description}}</p>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

