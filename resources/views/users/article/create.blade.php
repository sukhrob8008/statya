@extends('users.layouts.master')
@section('title','Maqola jo\'natish')
@section('content')
            <div class="page-body">
                        <div class="row">
                            <div class="col-sm-10 m-auto mt-5">
                                <div class="card">
                                    @if (\Session::has('msg'))
                                        <div class="alert alert-danger">
                                            <ul>
                                                <li>{!! \Session::get('msg') !!}</li>
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="card-header">
                                        <h5>Maqola yuborish</h5><span>Using the <a href="#">card</a> component, you can extend the default collapse behavior to create an accordion.</span>
                                    </div>
                                    <div class="card-body">
                                        <form id="formTaq" class="theme-form" method="post" action="{{ route('articles.store') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="mb-3">
                                                <label class="col-form-label pt-0" for="exampleInputEmail1">Maqola mavzusi</label>
                                                <input value="{{ old('articleTitle') }}" name="articleTitle" class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="Maqola mavzusi">
                                                @if($errors->has('articleTitle'))
                                                    <span class="text-danger">{{ $errors->first('articleTitle') }}</span>
                                                @endif
                                            </div>
                                            <div class="mb-3">
                                                <label class="col-form-label pt-0" for="exampleInputEmail">Mualliflar</label>
                                                <input value="{{ old('author') }}" name="author" class="form-control" id="exampleInputEmail" type="text" aria-describedby="emailHelp" placeholder="Mualliflar">
                                                @if($errors->has('author'))
                                                    <span class="text-danger">{{ $errors->first('author') }}</span>
                                                @endif
                                            </div>

                                            <div class="mb-3">
                                                <div class="col-form-label">Jurnalni tanlang</div>
                                                <select id="scien_id" name="scien_id" class="js-example-disabled-results col-sm-12">
                                                    <option value="null" selected disabled>{{ __('Jurnalni tanlang') }}</option>
                                                @foreach($sciens as $scien)
                                                    <option value="{{$scien->id}}">{{$scien->sciensName}}</option>
                                                @endforeach
                                                </select>
                                                @if($errors->has('scien_id'))
                                                    <span class="text-danger">{{ $errors->first('scien_id') }}</span>
                                                @endif
                                            </div>

                                            <div class="mb-3">
                                                <div class="col-form-label">Yo'nalishni tanlang</div>
                                                <select id="direct_id" name="direct_id" class="js-example-disabled-results col-sm-12">
                                                    <option value="null" selected disabled>{{ __('Select direct') }}</option>
{{--                                                    @foreach($directs as $direct)--}}
{{--                                                        <option data-journal="{{$direct->journal_id }}" value="{{$direct->id}}">{{$direct->journal_directs}}</option>--}}
{{--                                                    @endforeach--}}
                                                </select>
                                                @if($errors->has('direct_id'))
                                                    <span class="text-danger">{{ $errors->first('direct_id') }}</span>
                                                @endif
                                            </div>

                                            <div class="mb-3">
                                                <label for="exampleInputName1">Maqola haqida qisqacha</label>
                                                <textarea rows="15" name="articleAnotation" class="form-control" id="exampleInputName1">{{ old('articleAnotation') }}</textarea>
                                                @if($errors->has('articleAnotation'))
                                                    <span class="text-danger">{{ $errors->first('articleAnotation') }}</span>
                                                @endif
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputName1">Maqolaning Word faylini yuklang</label>
                                                <input value="{{ old('docFile') }}" name="docFile" class="form-control" type="file" aria-label="file example">
                                                @if($errors->has('docFile'))
                                                    <span class="text-danger">{{ $errors->first('docFile') }}</span>
                                                @endif
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputName1">Maqolaning PDF faylini yuklang</label>
                                                <input value="{{ old('pdfFile') }}" name="pdfFile" class="form-control" type="file" aria-label="file example">
                                                @if($errors->has('pdfFile'))
                                                    <span class="text-danger">{{ $errors->first('pdfFile') }}</span>
                                                @endif
                                            </div>
                                            <div class="mb-3 form">
                                                <label for="exampleInputName1">Taqriz faylini yuklang</label>
                                                <div class="row"  style="display: flex; align-items: center">
                                                    <div class="col-md-11">
                                                        <input value="{{ old('taqriz') }}" name="taqriz[]" class="form-control" type="file" aria-label="file example">
                                                        @if($errors->has('taqriz'))
                                                            <span class="text-danger">{{ $errors->first('taqriz') }}</span>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button id="addInput" class="btn btn-square btn-primary" type="button" data-toggle="tooltip" title="" role="button" data-bs-original-title="btn btn-primary"><i class="icofont icofont-ui-add"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <input type="submit" class="btn btn-primary" value="Maqolani yuborish"/>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

            </div>



@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#addInput").click(function (e) {
                e.preventDefault();
                $(".form").append(`
                     <div class="mb-3">

                        <div class="row" style="display: flex; align-items: center">
                            <label for="exampleInputName1">Taqriz faylini yuklang</label>
                            <div class="col-md-11">
                                <input value="{{ old('taqriz') }}" name="taqriz[]" class="form-control" type="file" aria-label="file example">
                                @if($errors->has('taqriz'))
                                    <span class="text-danger">{{ $errors->first('taqriz') }}</span>
                                @endif
                            </div>
                            <div class="col-md-1">
                                <button id="remove_item_btn" class="btn btn-square btn-danger" type="button" data-toggle="tooltip" title="" role="button" data-bs-original-title="btn btn-primary"><i class="icofont icofont-ui-delete"></i></button>
                            </div>
                        </div>
                    </div>
                `);
            });
            $(document).on('click', '#remove_item_btn', function (e) {
                e.preventDefault();
                let row_item = $(this).parent().parent();
                $(row_item).remove();
            });

            $("#scien_id").change(function(){
                $("#direct_id").html('');
                var scien_id = $(this).val();
                $("#direct_id").append('<option value="null" selected disabled>{{ __('Select direct') }}</option>');
                $.ajax({
                    url:'{{ route('ajax.journal')}}',
                    method: 'POST',
                    data:{
                        'scien_id' : scien_id
                    },
                    success:function (response) {
                        option='';
                        if(response.status === true){
                            $(response.directs).each(function( index, data ) {
                                $("#direct_id").append('<option value='+data.id+'>'+data.journal_directs+'</option>');
                            });
                        }
                    },
                    error:function (response) {
                        console.log(response)
                    }
                });
            });

        });

    </script>

@endpush
