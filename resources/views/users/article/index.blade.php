@extends('users.layouts.master')
@section('title', 'Jo\'natilgan maqolalar')
@section('content')
    <br><br>
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Jo'natilgan maqolalar</h5><span>Use a class<code>table</code> to any table.</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">Maqola mavzusi</th>
                                    <th scope="col">Uzatilgan vaqti</th>
                                    <th scope="col">Natija</th>
                                    <th scope="col">Maqola fayli</th>
                                    <th scope="col">Taqriz fayli</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $article)
                                    <tr>
                                        <th>{{$loop->index+1}}</th>
                                        <td>{{substr($article->articleTitle, 0, 20)."..."}}</td>
                                        <td>{{substr($article->created_at,0,10)}}</td>
                                        <td>
                                            @if($article->status == 0)
                                                <b style="color: #f1d12b">Tekshirilmoqda</b>
                                            @endif
                                            @if($article->status == 1)
                                                <b style="color: #04b10a">Tasdiqlandi</b>

                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal{{$article->id}}">
                                                        <i style="font-size: 15px; color: #04b10a" class="fa fa-info-circle"></i>
                                                    </a>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$article->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$article->id}}" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel{{$article->id}}">{{$article->articleTitle}}</h5>
{{--                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>--}}
                                                                </div>
                                                                <div class="modal-body">
                                                                    {{$article->messenger->messanger}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
                                                                    <a class="btn btn-success" href="https://checkout.paycom.uz/{{ base64_encode('m=641815b38325ae912936f157;ac.key='.$article->id.';a='.$article->price * 100) }}">pay me</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endif
                                            @if($article->status == -1)
                                                <b style="color: #f10808">Qaytarildi</b>
                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal{{$article->id}}">
                                                        <i style="font-size: 15px; color: #f10808" class="fa fa-info-circle"></i>
                                                    </a>

                                                    <!-- Modal -->
                                                    <div class="modal fade" id="exampleModal{{$article->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$article->id}}" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel{{$article->id}}">{{$article->articleTitle}}</h5>
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {{$article->messenger->messanger}}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish</button>
{{--                                                                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{asset('files/'.$article->docFormat)}}" class="btn btn-sm btn-primary" type="button" data-bs-toggle="tooltip" title="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>
                                            <a href="{{asset('files/'.$article->pdfFormat)}}" class="btn btn-sm btn-secondary" type="button" data-bs-toggle="tooltip" title="btn btn-secondary"><i class="fa fa-download"></i>&nbsp;&nbsp;PDF</a>
                                        </td>
                                        <td>
                                            @for($i=0; $i<count(explode(",",$article->taqriz)); $i++)
                                                <a href="{{asset('files/'.explode(",",$article->taqriz)[$i])}}" class="btn btn-sm btn-warning" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;Taqriz</a>
                                            @endfor
{{--                                            <a href="{{asset('files/'.$article->taqriz)}}" class="btn btn-info" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;Taqriz</a>--}}
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <div>{{ ($articles->count() > 0) ? $articles->links() : '' }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
