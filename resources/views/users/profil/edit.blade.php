@extends('users.layouts.master')
@section('title','Profil sozlash')
@section('content')
    <div class="login-card">
        <div>
            <div class="login-main">
                @if(\Illuminate\Support\Facades\Session::get('fail'))
                    <div class="alert alert-danger">
                        {{\Illuminate\Support\Facades\Session::get('fail')}}
                    </div>
                @endif
                <form class="theme-form" action="{{route('user.edit')}}" method="post">
                    @csrf
                    @method('PUT')
                    <h4>Profil sozlamalari</h4>
                    <div class="form-group">

                        <label class="col-form-label pt-0">Ism va Familiyangiz</label>
                        <div class="row g-2">
                            <div class="col-6">
                                <input name="name" value="{{$userProfil->name}}" class="form-control" type="text" required="" placeholder="Ismingiz">
                                @if($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif

                            </div>
                            <div class="col-6">
                                <input name="lname" value="{{$userProfil->lname}}" class="form-control" type="text" required="" placeholder="Familiyangiz">
                                @if($errors->has('lname'))
                                    <span class="text-danger">{{ $errors->first('lname') }}</span>
                                @endif

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Pochta manzilingiz</label>
                        <input name="email" value="{{$userProfil->email}}" class="form-control" type="email" required="" placeholder="Email">
                        @if($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif

                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Eski parol</label>
                        <div class="form-input position-relative">
                            <input class="form-control" type="password" name="old_pass" required="">
                            @if($errors->has('old_pass'))
                                <span class="text-danger">{{ $errors->first('old_pass') }}</span>
                            @endif

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Yangi parol</label>
                        <div class="form-input position-relative">
                            <input class="form-control" type="password" name="new_pass" required="">
                            @if($errors->has('new_pass'))
                                <span class="text-danger">{{ $errors->first('new_pass') }}</span>
                            @endif

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Parol takroran</label>
                        <div class="form-input position-relative">
                            <input class="form-control" type="password" name="new_pass_con" required="">
                            @if($errors->has('new_pass_con'))
                                <span class="text-danger">{{ $errors->first('new_pass_con') }}</span>
                            @endif

                        </div>
                    </div>
                    <div class="form-group mb-0">

                        <button class="btn btn-primary btn-block w-100" type="submit">Saqlash</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
