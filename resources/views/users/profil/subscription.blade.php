@extends('users.layouts.master')
@section('content')
    <div class="page-body">
    <div class="container-fluid credit-card mt-3">
        <div class="row" style="margin-top: 80px;">
            <div class="col-xxl-12 box-col-12" style="padding-top: 20px;">
                <div class="card">
                    <div class="card-header py-4">
                        <h5>Credit card </h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <form class="theme-form mega-form">
                                    <div class="mb-3">
                                        <input class="form-control" type="text" placeholder="Card number"
                                               data-bs-original-title="" title="">
                                    </div>
                                    <div class="mb-3">
                                        <input class="form-control" type="text" placeholder="First Name"
                                               data-bs-original-title="" title="">
                                    </div>
                                    <div class="mb-3">
                                        <input class="form-control" type="date" data-bs-original-title="" title="">
                                    </div>
                                    <div class="mb-3">
                                        <input class="form-control" type="text" placeholder="Name on card"
                                               data-bs-original-title="" title="">
                                    </div>
                                    <div class="mb-3">
                                        <select class="form-select" size="1">
                                            <option>Select Type</option>
                                            <option>Master</option>
                                            <option>Visa</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-5 text-center"><img class="img-fluid"
                                                                   src="{{ asset('users/assets/images/ecommerce/card.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
