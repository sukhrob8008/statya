{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
{{--<head>--}}

{{--    <meta charset="utf-8">--}}
{{--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">--}}
{{--    <title>@yield('title')</title>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />--}}
{{--    <link rel="stylesheet" href="{{asset('auth/assets/vendors/mdi/css/materialdesignicons.min.css')}}">--}}
{{--    <link rel="stylesheet" href="{{asset('auth/assets/vendors/css/vendor.bundle.base.css')}}">--}}

{{--    <link rel="stylesheet" href="{{asset('auth/assets/css/style.css')}}">--}}

{{--    <link rel="shortcut icon" href="{{asset('auth/assets/images/favicon.ico')}}" />--}}
{{--</head>--}}
{{--<body style="background: #efefef">--}}
{{--<div class="container-scroller">--}}

{{--    @include('admin.layouts.navbar')--}}

{{--    <div class="container-fluid page-body-wrapper">--}}

{{--        @include('admin.layouts.side')--}}

{{--        <div class="main-panel">--}}

{{--            @yield('content')--}}

{{--        </div>--}}

{{--    </div>--}}

{{--</div>--}}

{{--<script src="{{asset('auth/assets/vendors/js/vendor.bundle.base.js')}}"></script>--}}

{{--<script src="{{asset('auth/assets/vendors/chart.js/Chart.min.js')}}"></script>--}}
{{--<script src="{{asset('auth/assets/js/jquery.cookie.js')}}" type="text/javascript"></script>--}}

{{--<script src="{{asset('auth/assets/js/off-canvas.js')}}"></script>--}}
{{--<script src="{{asset('auth/assets/js/hoverable-collapse.js')}}"></script>--}}
{{--<script src="{{asset('auth/assets/js/misc.js')}}"></script>--}}

{{--<script src="{{asset('auth/assets/js/dashboard.js')}}"></script>--}}
{{--<script src="{{asset('auth/assets/js/todolist.js')}}"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>--}}
{{--@include('sweetalert::alert')--}}
{{--@stack('script')--}}
{{--</body>--}}
{{--</html>--}}
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('users//assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{asset('users/assets/images/favicon.png')}}" type="image/x-icon">
    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/font-awesome.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/icofont.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/themify.css')}}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/flag-icon.css')}}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/feather-icon.css')}}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/chartist.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/date-picker.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/select2.css')}}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/vendors/bootstrap.css')}}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/style.css')}}">
    <link id="color" rel="stylesheet" href="{{asset('users/assets/css/color-1.css')}}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{asset('users/assets/css/responsive.css')}}">
</head>
<body onload="startTime()">
<div class="loader-wrapper">
    <div class="loader-index"><span></span></div>
    <svg>
        <defs></defs>
        <filter id="goo">
            <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
            <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"> </fecolormatrix>
        </filter>
    </svg>
</div>

<div class="tap-top"><i data-feather="chevrons-up"></i></div>

<div class="page-wrapper compact-wrapper" id="pageWrapper">

    @include('admin.layouts.navbar')

    <div class="page-body-wrapper">

        @include('admin.layouts.side')

        @yield('content')

{{--        @include('admin.layouts.footer')--}}
    </div>
</div>

<script src="{{asset('users/assets/js/jquery-3.5.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{asset('users/assets/js/bootstrap/bootstrap.bundle.min.js')}}"></script>
<!-- feather icon js-->
<script src="{{asset('users/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{asset('users/assets/js/icons/feather-icon/feather-icon.js')}}"></script>
<!-- scrollbar js-->
<script src="{{asset('users/assets/js/scrollbar/simplebar.js')}}"></script>
<script src="{{asset('users/assets/js/scrollbar/custom.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{asset('users/assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<script src="{{asset('users/assets/js/sidebar-menu.js')}}"></script>
<script src="{{asset('users/assets/js/chart/chartist/chartist.js')}}"></script>
<script src="{{asset('users/assets/js/chart/chartist/chartist-plugin-tooltip.js')}}"></script>
<script src="{{asset('users/assets/js/chart/knob/knob.min.js')}}"></script>
<script src="{{asset('users/assets/js/chart/knob/knob-chart.js')}}"></script>
<script src="{{asset('users/assets/js/chart/apex-chart/apex-chart.js')}}"></script>
<script src="{{asset('users/assets/js/chart/apex-chart/stock-prices.js')}}"></script>
<script src="{{asset('users/assets/js/notify/bootstrap-notify.min.js')}}"></script>
<script src="{{asset('users/assets/js/dashboard/default.js')}}"></script>
<script src="{{asset('users/assets/js/notify/index.js')}}"></script>
<script src="{{asset('users/assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('users/assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('users/assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('users/assets/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('users/assets/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('users/assets/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('users/assets/js/typeahead-search/handlebars.js')}}"></script>
<script src="{{asset('users/assets/js/typeahead-search/typeahead-custom.js')}}"></script>
<script src="{{asset('users/assets/js/select2/select2.full.min.js')}}"></script>
<script src="{{asset('users/assets/js/select2/select2-custom.js')}}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{asset('users/assets/js/script.js')}}"></script>
<script src="{{asset('users/assets/js/theme-customizer/customizer.js')}}"></script>
@stack('scripts')
<!-- login js-->
<!-- Plugin used-->
</body>
</html>
