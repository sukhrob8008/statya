{{--{{session_start()}}--}}
{{--<nav class="sidebar sidebar-offcanvas" id="sidebar">--}}
{{--    <ul class="nav">--}}
{{--        <li class="nav-item nav-profile">--}}
{{--            <a href="#" class="nav-link">--}}
{{--                <div class="nav-profile-image">--}}
{{--                    <img src="{{asset('auth/assets/images/faces/face1.jpg')}}" alt="profile">--}}
{{--                    <span class="login-status online"></span>--}}
{{--                    <!--change to offline or busy as needed-->--}}
{{--                </div>--}}
{{--                <div class="nav-profile-text d-flex flex-column">--}}
{{--                    <span class="font-weight-bold mb-2">{{$ism}}</span>--}}
{{--                    <span class="font-weight-bold mb-2">{{\Illuminate\Support\Facades\Auth::user()->name." ".\Illuminate\Support\Facades\Auth::user()->lname}}</span>--}}
{{--                    <span class="text-secondary text-small">Project Manager</span>--}}
{{--                </div>--}}
{{--                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" href="{{route('admin.dashboard')}}">--}}
{{--                <span class="menu-title">Dashboard</span>--}}
{{--                <i class="mdi mdi-home menu-icon"></i>--}}
{{--            </a>--}}
{{--        </li>--}}
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" data-bs-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">--}}
{{--                <span class="menu-title">Fan yo'nalishi</span>--}}
{{--                <i class="menu-arrow"></i>--}}
{{--                <i class="mdi mdi mdi-briefcase menu-icon"></i>--}}
{{--            </a>--}}
{{--            <div class="collapse" id="ui-basic">--}}
{{--                <ul class="nav flex-column sub-menu">--}}
{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('sciens.create')}}">Yo'nalish qo'shish</a></li>--}}

{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('sciens.index')}}">Yo'nalishlar</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </li>--}}

{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" data-bs-toggle="collapse" href="#ui-rules" aria-expanded="false" aria-controls="ui-rules">--}}
{{--                <span class="menu-title">Qoidalar</span>--}}
{{--                <i class="menu-arrow"></i>--}}
{{--                <i class="mdi mdi mdi-script menu-icon"></i>--}}
{{--            </a>--}}
{{--            <div class="collapse" id="ui-rules">--}}
{{--                <ul class="nav flex-column sub-menu">--}}
{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('rules.create')}}">Qoida qo'shish</a></li>--}}

{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('rules.index')}}">Qoida tahrirlash</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </li>--}}

{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" data-bs-toggle="collapse" href="#ui-rules_3" aria-expanded="false" aria-controls="ui-rules_3">--}}
{{--                <span class="menu-title">Maqolalar</span>--}}
{{--                <i class="menu-arrow"></i>--}}
{{--                <i class="mdi mdi mdi-script menu-icon"></i>--}}
{{--            </a>--}}
{{--            <div class="collapse" id="ui-rules_3">--}}
{{--                <ul class="nav flex-column sub-menu">--}}
{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('user.success')}}">Tasdiqlangan maqolalar</a></li>--}}

{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('user.timesuccess')}}">vaqt oraliqdagi</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </li>--}}

{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" data-bs-toggle="collapse" href="#ui-rules2" aria-expanded="false" aria-controls="ui-basic">--}}
{{--                <span class="menu-title">Maqolaning navbatdagi<br> soni</span>--}}
{{--                <i class="menu-arrow"></i>--}}
{{--                <i class="mdi mdi mdi-script menu-icon"></i>--}}
{{--            </a>--}}
{{--            <div class="collapse" id="ui-rules2">--}}
{{--                <ul class="nav flex-column sub-menu">--}}
{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('maqolas.create')}}">Navbatdagi sonni <br> qo'shish</a></li>--}}

{{--                    <li class="nav-item"> <a class="nav-link" href="{{route('maqolas.index')}}">Navbatdagi sonlar</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </li>--}}

{{--    </ul>--}}
{{--</nav>--}}

<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="{{route('user.dashboard')}}"><img class="img-fluid for-light" src="{{asset('users/assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{asset('users/assets/images/logo/logo_dark.png')}}" alt=""></a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
        </div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="{{asset('users/assets/images/logo/logo-icon.png')}}" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="{{asset('users/assets/images/logo/logo-icon.png')}}" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">--}}
                        <div>
                            <h6 class="">Mjournal</h6>
                            <p class="">Buxoro Davlat Universiteti</p>
                        </div>
                    </li>

                    <li class="sidebar-list">
{{--                        <label class="badge badge-danger">Yangi</label>--}}
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="briefcase"></i>
                            <span>Jurnallar</span></a>
                        <ul class="sidebar-submenu {{(request()->is('admin/sciens/create') || request()->is('admin/sciens') || request()->is('admin/sciens/*/edit')) ? 'd-block' : ''}}">
                            <li><a style="color: {{request()->is('admin/sciens/create') ? '#7366ff' : ''}}" href="{{route('sciens.create')}}">Jurnal qo'shish</a></li>
                            <li><a style="color: {{( request()->is('admin/sciens') || request()->is('admin/sciens/*/edit') ) ? '#7366ff' : ''}}" href="{{route('sciens.index')}}">Jurnal ko'rish</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="grid"></i>
                            <span>Yo'nalishlar</span></a>
                        <ul class="sidebar-submenu {{(request()->is('admin/direct/create') || request()->is('admin/direct')) ? 'd-block' : ''}}">
                            <li><a style="color: {{request()->is('admin/direct/create') ? '#7366ff' : ''}}" href="{{route('direct.create')}}">Yo'nalish qo'shish</a></li>
                            <li><a style="color: {{request()->is('admin/direct') ? '#7366ff' : ''}}" href="{{route('direct.index')}}">Yo'nalish ko'rish</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
{{--                        <label class="badge badge-danger">Yangi</label>--}}
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="bookmark"></i>
                            <span>Qoidalar</span>
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('admin/rules/create') || request()->is('admin/rules')) ? 'd-block' : ''}}">
                            <li><a style="color: {{request()->is('admin/rules/create') ? '#7366ff' : ''}}" href="{{route('rules.create')}}">Qoida qo'shish</a></li>
                            <li><a style="color: {{request()->is('admin/rules') ? '#7366ff' : ''}}" href="{{route('rules.index')}}">Qoida tahrirlash</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
                        <label class="badge badge-danger">Yangi</label>
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="file-text"></i>
                            <span>Maqolalar</span>
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('admin/timesuccess') || request()->is('admin/success')) ? 'd-block' : ''}}">
                            <li><a style="color: {{request()->is('admin/success') ? '#7366ff' : ''}}" href="{{route('user.success')}}">Tasdiqlangan maqolalar</a></li>
                            <li><a style="color: {{request()->is('admin/timesuccess') ? '#7366ff' : ''}}" href="{{route('user.timesuccess')}}">Vaqt oraliqdagi</a></li>
                        </ul>
                    </li>

                    <li class="sidebar-list">
{{--                        <label class="badge badge-danger">Yangi</label>--}}
                        <a class="sidebar-link sidebar-title" href="#">
                            <i data-feather="clock"></i>
                            <span>Maqola soni</span>
                        </a>
                        <ul class="sidebar-submenu {{(request()->is('admin/maqolas/create') || request()->is('admin/maqolas') || request()->is('admin/maqolas/*/edit')) ? 'd-block' : ''}}">
                            <li><a style="color: {{request()->is('admin/maqolas/create') ? '#7366ff' : ''}}" href="{{route('maqolas.create')}}">Yangi son</a></li>
                            <li><a style="color: {{(request()->is('admin/maqolas') || request()->is('admin/maqolas/*/edit')) ? '#7366ff' : ''}}" href="{{route('maqolas.index')}}">Navbatdagi sonlar</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
