@extends('admin.layouts.master')
@section('title','Izoh yuborish')
@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                <div class="card-body">
                    {{--                    {{dd($user_id)}}--}}
                    <h3>{{$user_id->articleTitle}}</h3>
                    <h6>Mavzusiga oid <span style="border-radius: 5px; padding: 2px 8px; color: white" class="bg-danger">qaytarilgan</span> maqola uchun uzatuvchi <span style="border-radius: 5px; padding: 2px 8px; color: white" class="bg-danger">{{$user_id->users->lname." ".$user_id->users->name}}ga</span> izoh jo'natish</h6>

                    <form action="{{route('messanger.destroy', $user_id)}}" method="post" class="forms-sample">
                        @csrf
                        @method('DELETE')
                        <div class="form-group">
                            <label for="exampleInputName1">Izoh!</label>
                            <textarea rows="15" name="messanger" class="form-control" id="exampleInputName1"></textarea>
                            @if($errors->has('messanger'))
                                <span class="text-danger">{{ $errors->first('messanger') }}</span>
                            @endif
                        </div>
                        <input type="hidden" name="user_id" value="{{$user_id->user_id}}">
                        <input type="hidden" name="article_id" value="{{$user_id->id}}">
                        <div class="mt-3">
                            <button class="btn btn-danger" type="submit">Maqola egasiga yuborish</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
