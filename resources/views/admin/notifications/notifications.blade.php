@extends('admin.layouts.master')
@section('title','Xabarlar')
@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Kelgan maqolalar</h4>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Yuborgan shaxs</th>
                            <th>Yuborilgan vaqti</th>
                            <th>doc fayl</th>
                            <th>pdf fayl</th>
                            <th>taqriz fayl</th>
                            <th>Tasdiqlash</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{$article->users->lname." ".$article->users->name}}</td>
                                <td>{{substr($article->created_at,0,10)}}</td>
                                <td>
                                    <a href="{{asset('files/'.$article->docFormat)}}" class="btn btn-sm btn-primary" type="button" data-bs-toggle="tooltip" title="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>
                                </td>
                                <td>
                                    <a href="{{asset('files/'.$article->pdfFormat)}}" class="btn btn-sm btn-danger" type="button" data-bs-toggle="tooltip" title="btn btn-secondary"><i class="fa fa-download"></i>&nbsp;&nbsp;PDF</a>
                                </td>
                                <td>
                                    @for($i=0; $i<count(explode(",",$article->taqriz)); $i++)
                                        <a href="{{asset('files/'.explode(",",$article->taqriz)[$i])}}" class="btn btn-sm btn-warning" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;Taqriz</a>
                                    @endfor
                                </td>
                                <td>
                                    <a href="{{route('messanger.show', $article)}}" class="d-inline-block btn btn-sm btn-success"><i class="fa-solid mdi mdi-bookmark-check"></i>&nbsp;&nbsp;Tasdiqlash</a>
                                    <a href="{{route('messanger.edit', $article)}}" class="d-inline-block btn btn-sm btn-danger"><i class="fa-solid mdi mdi-bookmark-remove"></i>&nbsp;&nbsp;Qaytarish</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>{{ ($articles->count() > 0) ? $articles->links() : '' }}</div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
