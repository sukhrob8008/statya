@extends('admin.layouts.master')
@section('title','Xabarlar')
@section('content')
    {{--    <div class="container mt-5">--}}

    {{--        <div class="col-lg-12 grid-margin stretch-card">--}}

    {{--            <div class="card">--}}
    {{--                @if(\Illuminate\Support\Facades\Session::get('success'))--}}
    {{--                    <div class="alert alert-success">--}}
    {{--                        {{\Illuminate\Support\Facades\Session::get('success')}}--}}
    {{--                    </div>--}}
    {{--                @endif--}}
    {{--                <div class="card-body">--}}
    {{--                    <h4 class="card-title">Tasdiqlangan maqolalar</h4>--}}
    {{--                    <table class="table">--}}
    {{--                        <thead>--}}
    {{--                        <tr>--}}
    {{--                            <th>№</th>--}}
    {{--                            <th>Yuborgan shaxs</th>--}}
    {{--                            <th>Maqola Soni</th>--}}
    {{--                            <th>Yuborilgan vaqti</th>--}}
    {{--                            <th>doc fayl</th>--}}
    {{--                            <th>pdf fayl</th>--}}
    {{--                            <th>taqriz fayl</th>--}}
    {{--                            <th>Tasdiqlash</th>--}}
    {{--                        </tr>--}}
    {{--                        </thead>--}}
    {{--                        <tbody>--}}

{{--                            @foreach($articles as $article)--}}
{{--                                    @if($maqolas->show_date <= date('Y-m-d', time()))--}}
{{--                                    @if($article->file != null)--}}
{{--                                        <tr>--}}
{{--                                            <td>{{$loop->index+1}}</td>--}}
{{--                                            <td>{{$article->users->lname." ".$article->users->name}}</td>--}}
{{--                                            <td>{{$article->publication}}</td>--}}
{{--                                            <td>{{substr($article->created_at,0,10)}}</td>--}}
{{--                                            <td>--}}
{{--                                                <a href="{{asset('files/'.$article->docFormat)}}" class="btn btn-sm btn-primary" type="button" data-bs-toggle="tooltip" title="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>--}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                <a href="{{asset('files/'.$article->pdfFormat)}}" class="btn btn-sm btn-danger" type="button" data-bs-toggle="tooltip" title="btn btn-secondary"><i class="fa fa-download"></i>&nbsp;&nbsp;PDF</a>--}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                <a href="{{asset('files/'.$article->taqriz)}}"    class="btn btn-sm btn-warning" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;Taqriz</a>--}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
{{--                                    @endif--}}
{{--                            @endforeach--}}
    {{--                        </tbody>--}}
{{--                            <div>{{ ($articles->count() > 0) ? $articles->links() : '' }}</div>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="page-body pt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if(\Illuminate\Support\Facades\Session::get('success'))
                            <div class="alert alert-success">
                                {{\Illuminate\Support\Facades\Session::get('success')}}
                            </div>
                        @endif
                        <div class="card-header">
                            <h4 class="card-title">Tasdiqlangan maqolalar</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Yuborgan shaxs</th>
                                    <th>Maqola Soni</th>
                                    <th>Yuborilgan vaqti</th>
                                    <th>doc fayl</th>
                                    <th>pdf fayl</th>
                                    <th>taqriz fayl</th>
                                    <th>Tasdiqlash</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($articles as $article)
                                    @if($maqolas->show_date <= date('Y-m-d', time()))
                                        @if($article->file != null)
                                            <tr>
                                                <td>{{$loop->index+1}}</td>
                                                <td>{{$article->users->lname." ".$article->users->name}}</td>
                                                <td>{{$article->publication}}</td>
                                                <td>{{substr($article->created_at,0,10)}}</td>
                                                <td>
                                                    <a href="{{asset('files/'.$article->docFormat)}}" class="btn btn-sm btn-primary" type="button" data-bs-toggle="tooltip" title="btn btn-primary"><i class="fa fa-download"></i>&nbsp;&nbsp;DOC</a>
                                                </td>
                                                <td>
                                                    <a href="{{asset('files/'.$article->pdfFormat)}}" class="btn btn-sm btn-danger" type="button" data-bs-toggle="tooltip" title="btn btn-secondary"><i class="fa fa-download"></i>&nbsp;&nbsp;PDF</a>
                                                </td>
                                                <td>
                                                    <a href="{{asset('files/'.$article->taqriz)}}"    class="btn btn-sm btn-warning" type="button" data-bs-toggle="tooltip" title="btn btn-info"><i class="fa fa-download"></i>&nbsp;&nbsp;Taqriz</a>
                                                </td>
                                            </tr>
                                        @endif
                                        @endif
                                        @endforeach
                                </tbody>
                                <div>{{ ($articles->count() > 0) ? $articles->links() : '' }}</div>
                            </table>
                            {{--<div>{{ ($sciens->count() > 0) ? $sciens->links() : '' }}</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
