@extends('admin.layouts.master')
@section('title','Fan yo\'nalishlari')
@section('content')

{{--    <div class="container mt-5">--}}

{{--        <div class="col-lg-12 grid-margin stretch-card">--}}

{{--            <div class="card">--}}
{{--                @if(\Illuminate\Support\Facades\Session::get('success'))--}}
{{--                    <div class="alert alert-success">--}}
{{--                        {{\Illuminate\Support\Facades\Session::get('success')}}--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--                <div class="card-body">--}}
{{--                    <h4 class="card-title">Fan yo'nalishlari jadvali</h4>--}}
{{--                    <table class="table">--}}
{{--                        <thead>--}}
{{--                        <tr>--}}
{{--                            <th>№</th>--}}
{{--                            <th>Yo'nalish nomi</th>--}}
{{--                            <th>Yaratilgan vaqti</th>--}}
{{--                            <th>Tahrirlash</th>--}}
{{--                        </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                        @foreach($sciens as $scien)--}}
{{--                            <tr>--}}
{{--                                <td>{{$loop->index+1}}</td>--}}
{{--                                <td>{{$scien->sciensName}}</td>--}}
{{--                                <td>{{$scien->created_at}}</td>--}}
{{--                                <td>--}}
{{--                                    <a href="{{route('sciens.edit', $scien)}}" class="d-inline-block btn btn-primary"><i class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;O'zgartirish</a>--}}
{{--                                    <form class="d-inline-block" action="{{route('sciens.destroy', $scien->id)}}" method="POST">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button type="submit" class="btn btn-danger"><i class="fa-solid fa-trash-arrow-up"></i>&nbsp;&nbsp; O'chirish</button>--}}
{{--                                        <button type="submit" class="btn btn-danger show_confirm" data-toggle="tooltip" title='Delete'><i class="far fa-trash-alt"></i>&nbsp;&nbsp;O'chirish</button>--}}

{{--                                    </form>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
<br><br>
<div class="page-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Jurnallar ro'yxati</h5><span>Use a class<code>table</code> to any table.</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">Jurnal nomi</th>
                                <th scope="col">Jurnal yaratilgan vaqti</th>
                                <th scope="col">Tahrirlash</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sciens as $scien)
                                <tr>
                                    <th>{{$loop->index+1}}</th>
                                    <td>{{$scien->sciensName}}</td>
                                    <td>{{$scien->created_at}}</td>
                                    <td>
                                        <a href="{{route('sciens.edit', $scien)}}"
                                           class="d-inline-block btn btn-primary"><i
                                                class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;O'zgartirish</a>
                                        <form class="d-inline-block" action="{{route('sciens.destroy', $scien->id)}}"
                                              method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger show_confirm"
                                                    data-toggle="tooltip" title='Delete'><i
                                                    class="far fa-trash-alt"></i>&nbsp;&nbsp;O'chirish
                                            </button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
{{--                        <div>{{ ($sciens->count() > 0) ? $sciens->links() : '' }}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Siz yo\'nalishni o\'chirmoqchimisiz?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
