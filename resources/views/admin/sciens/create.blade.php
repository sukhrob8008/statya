@extends('admin.layouts.master')
@section('title', 'Yo\'nalish qo\'shish')
@section('content')
{{--    <div class="page-body">--}}
{{--        <div class="container mt-5">--}}
{{--            <div class="col-12 grid-margin stretch-card">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <h4 class="card-title">Fan yo'nalishini</h4>--}}
{{--                            <form action="{{route('sciens.store')}}" method="post" class="forms-sample">--}}
{{--                                @csrf--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputName1">Nomi</label>--}}
{{--                                    <input name="sciensName" type="text" class="form-control" id="exampleInputName1" placeholder="Fan yo'nalishi nomi">--}}
{{--                                    <span class="text-danger">@error('sciensName'){{ $message }} @enderror</span>--}}
{{--                                </div>--}}
{{--                                <button type="submit" class="btn btn-gradient-primary me-2">Qo'shish</button>--}}
{{--                            </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
<div class="page-body">
    <div class="row">
        <div class="col-sm-12 m-auto mt-5">
            <div class="card">
                <div class="card-header">
                    <h5>Yo'nalish yaratish</h5>
                </div>
                <div class="card-body">
                    <form id="formTaq" class="theme-form" method="post" action="{{route('sciens.store')}}">
                        @csrf
                        <div class="mb-3">
                            <label class="col-form-label pt-0" for="exampleInputEmail1">Yo'nalish nomi</label>
                            <input name="sciensName" class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="Yo'nalish nomi">
                            <span class="text-danger">@error('sciensName'){{ $message }} @enderror</span>
                        </div>
                        <div class="mb-3">
                            <input type="submit" class="btn btn-primary" value="Yo'nalish yaratish"/>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection
