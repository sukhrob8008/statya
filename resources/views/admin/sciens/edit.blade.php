@extends('admin.layouts.master')
@section('title', 'Yo\'nalishni o\'zgartirish')
@section('content')

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Jurnal nomini tahrirlash</h4>
                    </div>
                    <div class="card-body">
                        <form class="forms-sample" action="{{route('sciens.update', $scien)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="mb-3 form-group">
                                <label for="exampleInputUsername1">Jurnal nomi</label>
                                <input value="{{$scien->sciensName}}" name="sciensName" type="text" class="form-control" id="exampleInputUsername1" placeholder="Yo\'nalish nomi">
                            </div>

                            <div class="mb-3">
                                <button class="btn btn-primary" type="submit">Jurnalni o'zgartirish</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

