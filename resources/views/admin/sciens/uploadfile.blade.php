@extends('admin.layouts.master')
@section('title', 'Fayl')
@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Faylni yuklang!</h4>

                        <form action="{{route('user.uploadfilestore', $article)}}" method="post" class="forms-sample" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="file_upload">Maqolaning qirqilgan PDF faylini yuklang</label>
                                <input value="" name="file_upload" class="form-control" type="file" aria-label="file example">
                                @if($errors->has('file_upload'))
                                    <span class="text-danger">{{ $errors->first('pdfFile') }}</span>
                                @endif
                            </div>

                            <div class="mt-3">
                                <input type="submit" class="btn btn-primary" value="Qo'shish"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('auth/assets/js/file-upload.js') }}"></script>
@endpush
