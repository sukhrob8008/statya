 @extends('admin.layouts.master')
@section('title','Qoida tahrirlash')
@section('content')


<div class="page-body pt-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @if(\Illuminate\Support\Facades\Session::get('success'))
                        <div class="alert alert-success">
                            {{\Illuminate\Support\Facades\Session::get('success')}}
                        </div>
                    @endif
                    <div class="card-header">
                        <h4 class="card-title">Qoida jadvali</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col" class="col-1">№</th>
                                <th scope="col" class="col-1">Qoida sarlavhasi</th>
                                <th scope="col" class="col-3">Qoida asosiy mazmuni</th>
                                <th scope="col" class="col-1">Tahrirlash</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rules as $rule)
                                <tr>
                                        <td>
                                            {{$loop->index+1}}
                                        </td>
                                        <td>
                                            {{$rule->title}}
                                        </td>
                                        <td style="text-align: justify">
                                            {{$rule->description}}
                                        </td>
                                        <td class="d-flex align-content-center">
                                            <a style="margin-right: 10px;" href="" class="d-inline-block btn btn-primary"><i class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;O'zgartirish</a>
                                            <form class="d-inline-block" action="{{route('rules.destroy', $rule->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="d-flex btn btn-danger" data-toggle="tooltip" title='Delete'><i class="far fa-trash-alt"></i>&nbsp;&nbsp;O'chirish</button>
                                            </form>
                                        </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        {{--<div>{{ ($sciens->count() > 0) ? $sciens->links() : '' }}</div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Siz qoidani o\'chirmoqchimisiz?`,

                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
