@extends('admin.layouts.master')
@section('title','Qoida yaratish')
@section('content')
{{--    <div class="container mt-5">--}}
{{--    <div class="col-12 grid-margin stretch-card">--}}
{{--        <div class="card">--}}
{{--            <div class="card-body">--}}
{{--                <h4 class="card-title">Maqola yuborish uchun qoida yaratish</h4>--}}

{{--                <form action="{{route('rules.store')}}" method="post" class="forms-sample">--}}
{{--                    @csrf--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="exampleInputName1">Qoida sarlavhasi</label>--}}
{{--                        <input name="title" type="text" class="form-control" id="exampleInputName1" placeholder="Qoida sarlavhasi">--}}
{{--                        @if($errors->has('title'))--}}
{{--                            <span class="text-danger">{{ $errors->first('title') }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="exampleInputName1">Asosiy mazmuni</label>--}}
{{--                        <input name="description" type="text" class="form-control" id="exampleInputName1" placeholder="Qoida sarlavhasi">--}}
{{--                        <textarea rows="15" name="description" class="form-control" id="exampleInputName1"></textarea>--}}
{{--                        @if($errors->has('description'))--}}
{{--                            <span class="text-danger">{{ $errors->first('description') }}</span>--}}
{{--                        @endif--}}
{{--                    </div>--}}

{{--                    <button type="submit" class="btn btn-gradient-primary me-2">Qo'shish</button>--}}

{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    </div>--}}

<div class="page-body">
    <div class="row">
        <div class="col-sm-12 m-auto mt-5">
            <div class="card">
                <div class="card-header">
                    <h5>Maqola yuborish uchun qoida yaratish</h5>
                </div>
                <div class="card-body">
                    <form id="formTaq" class="theme-form" method="post" action="{{route('rules.store')}}">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputName1" class="col-form-label pt-0">Qoida sarlavhasi</label>
                            <input name="title" type="text" class="form-control" id="exampleInputName1" placeholder="Qoida sarlavhasi">
                            @if($errors->has('title'))
                                <span class="text-danger">{{ $errors->first('title') }}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <label for="exampleInputName1" class="col-form-label pt-0">Asosiy mazmuni</label>
                            <textarea rows="15" name="description" class="form-control" id="exampleInputName1"></textarea>
                            @if($errors->has('description'))
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <input type="submit" class="btn btn-primary" value="Qo'shish"/>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection
