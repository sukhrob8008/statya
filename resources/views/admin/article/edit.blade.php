@extends('admin.layouts.master')
@section('title','Maqolaning navbatdagi sonini tahrirlash')
@section('content')
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Maqola yuborish uchun qoida yaratish</h4>

                    <form action="{{route('maqolas.update', $maqola)}}" method="post" class="forms-sample">
                        @csrf
                        @method('PUT')
                        <div class="form-group mb-3">
                            <label for="publication">Maqolani nashri soni</label>
                            <input name="publication" type="number" class="form-control" id="publication" value="{{ $maqola->publication }}">
                            @if($errors->has('publication'))
                                <span class="text-danger">{{ $errors->first('publication') }}</span>
                            @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="exampleInputName1">Maqolani boshlash vaqti</label>
                            <input name="start_date" type="date" class="form-control" id="exampleInputName1" value="{{ $maqola->start_date }}">
                            @if($errors->has('start_date'))
                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                            @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="exampleInputName2">Maqolani tugash vaqti</label>
                            <input name="end_date" type="date" class="form-control" id="exampleInputName2" value="{{ $maqola->end_date }}">
                            @if($errors->has('end_date'))
                                <span class="text-danger">{{ $errors->first('end_date') }}</span>
                            @endif
                        </div>
                        <div class="form-group mb-3">
                            <label for="exampleInputName3">Maqolani chiqish vaqti</label>
                            <input name="show_date" type="date" class="form-control" id="exampleInputName3" value="{{ $maqola->show_date }}">
                            @if($errors->has('show_date'))
                                <span class="text-danger">{{ $errors->first('show_date') }}</span>
                            @endif
                        </div>
                        <input type="hidden" name="year" value="{{ date('Y') }}">

                        <div class="mb-3">
                            <button class="btn btn-primary" type="submit">Vaqtlarni o'zgartirish</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
