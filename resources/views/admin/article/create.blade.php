@extends('admin.layouts.master')
@section('title','Maqolaning navbatdagi sonini yaratish')
@section('content')
{{--    <div class="container mt-5">--}}
{{--        <div class="col-12 grid-margin stretch-card">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                    @if(session()->has('messageM'))--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            {{ session()->get('messageM') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    <h4 class="card-title">Maqola yuborish uchun qoida yaratish</h4>--}}

{{--                    <form action="{{route('maqolas.store')}}" method="post" class="forms-sample">--}}
{{--                        @csrf--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="publication">Maqolani nashri soni</label>--}}
{{--                            <input name="publication" type="number" class="form-control" id="publication">--}}
{{--                            @if($errors->has('publication'))--}}
{{--                                <span class="text-danger">{{ $errors->first('publication') }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <label for="exampleInputName1">Maqolani boshlash vaqti</label>--}}
{{--                            <input name="start_date" type="date" class="form-control" id="exampleInputName1">--}}
{{--                            @if($errors->has('start_date'))--}}
{{--                                <span class="text-danger">{{ $errors->first('start_date') }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="exampleInputName2">Maqolani tugash vaqti</label>--}}
{{--                            <input name="end_date" type="date" class="form-control" id="exampleInputName2">--}}
{{--                            @if($errors->has('end_date'))--}}
{{--                                <span class="text-danger">{{ $errors->first('end_date') }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="exampleInputName3">Maqolani chiqish vaqti</label>--}}
{{--                            <input name="show_date" type="date" class="form-control" id="exampleInputName3">--}}
{{--                            @if($errors->has('show_date'))--}}
{{--                                <span class="text-danger">{{ $errors->first('show_date') }}</span>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                        <input type="hidden" name="year" value="{{ date('Y') }}">--}}
{{--                        <button type="submit" class="btn btn-gradient-primary me-2">Qo'shish</button>--}}

{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}



<div class="page-body">
    <div class="row">
        <div class="col-sm-12 m-auto mt-5">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Maqola yuborish uchun qoida yaratish</h4>
                </div>
                <div class="card-body">
                    @if(session()->has('messageM'))
                        <div class="alert alert-danger">
                            {{ session()->get('messageM') }}
                        </div>
                    @endif
                        <form action="{{route('maqolas.store')}}" method="post" class="forms-sample">
                            @csrf
                            <div class="mb-3">
                                <label for="publication">Maqolani nashri soni</label>
                                <input name="publication" type="number" class="form-control" id="publication">
                                @if($errors->has('publication'))
                                    <span class="text-danger">{{ $errors->first('publication') }}</span>
                                @endif
                            </div>

                            <div class="mb-3">
                                <label for="exampleInputName1">Maqolaga qabul boshlanish vaqti</label>
                                <input name="start_date" type="date" class="form-control" id="exampleInputName1">
                                @if($errors->has('start_date'))
                                    <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputName2">Maqolaga qabul tugash vaqti</label>
                                <input name="end_date" type="date" class="form-control" id="exampleInputName2">
                                @if($errors->has('end_date'))
                                    <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                @endif
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputName3">Maqolani jurnalga chop qilinish vaqti</label>
                                <input name="show_date" type="date" class="form-control" id="exampleInputName3">
                                @if($errors->has('show_date'))
                                    <span class="text-danger">{{ $errors->first('show_date') }}</span>
                                @endif
                            </div>
                            <input type="hidden" name="year" value="{{ date('Y') }}">
                            <div class="mb-3">
                                <input type="submit" class="btn btn-primary" value="Qo'shish"/>
                            </div>

                        </form>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection
