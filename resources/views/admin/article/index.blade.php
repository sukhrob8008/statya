@extends('admin.layouts.master')
@section('title','Maqola sonlarini tahrirlash')
@section('content')

    {{--    <div class="container mt-5">--}}

    {{--        <div class="col-lg-12 grid-margin stretch-card">--}}

    {{--            <div class="card" style="padding: 20px;">--}}
    {{--                @if(\Illuminate\Support\Facades\Session::get('success'))--}}
    {{--                    <div class="alert alert-success">--}}
    {{--                        {{\Illuminate\Support\Facades\Session::get('success')}}--}}
    {{--                    </div>--}}
    {{--                @endif--}}
    {{--                <td class="card-body">--}}
    {{--                    @if(session()->has('messageM'))--}}
    {{--                        <div class="alert alert-danger">--}}
    {{--                            {{ session()->get('messageM') }}--}}
    {{--                        </div>--}}
    {{--                    @endif--}}
    {{--                    <h4 class="card-title">Maqolalar soni jadvali</h4>--}}
    {{--                    <br>--}}
    {{--                    <table class="table">--}}
    {{--                        <thead>--}}
    {{--                        <tr>--}}

    {{--                            <th>--}}
    {{--                                №--}}
    {{--                            </th>--}}
    {{--                            <th>--}}
    {{--                                Nashr soni--}}
    {{--                            </th>--}}
    {{--                            <th>--}}
    {{--                                Boshlanish vaqti--}}
    {{--                            </th>--}}
    {{--                            <th>--}}
    {{--                                Tugash vaqti--}}
    {{--                            </th>--}}
    {{--                            <th>--}}
    {{--                                Chiqish vaqti--}}
    {{--                            </th>--}}

    {{--                            <th>--}}
    {{--                                Yili--}}
    {{--                            </th>--}}

    {{--                            <th>--}}
    {{--                                Tahrirlash--}}
    {{--                            </th>--}}


    {{--                        </tr>--}}

    {{--                        </thead>--}}

    {{--                        <tbody>--}}
{{--                            @foreach($maqolas as $maqola)--}}
{{--                                <tr>--}}

{{--                                    <td>--}}
{{--                                        {{$loop->index+1}}--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        {{$maqola->publication}}--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        {{$maqola->start_date}}--}}
{{--                                    </td>--}}
{{--                                    <td style="text-align: justify">--}}
{{--                                        {{$maqola->end_date}}--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        {{ $maqola->show_date }}--}}
{{--                                    </td>--}}

{{--                                    <td>--}}
{{--                                        {{ $maqola->year }}--}}
{{--                                    </td>--}}
{{--                                    <td class="h-25 d-flex align-content-center">--}}
{{--                                        <a href="{{ route('maqolas.edit', $maqola) }}" class="d-flex btn btn-primary"--}}
{{--                                           style="margin-right: 15px;"><i class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;O'zgartirish</a><br>--}}
{{--                                        <form class="d-flex" action="{{route('maqolas.destroy', $maqola) }}" method="POST">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button type="submit" class="d-flex btn btn-danger" data-toggle="tooltip"--}}
{{--                                                    title='Delete'><i class="far fa-trash-alt"></i>&nbsp;&nbsp;O'chirish--}}
{{--                                            </button>--}}
{{--                                        </form>--}}
{{--                                    </td>--}}


{{--                                </tr>--}}
{{--                            @endforeach--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    </div>--}}

    <div class="page-body pt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        @if(\Illuminate\Support\Facades\Session::get('success'))
                            <div class="alert alert-success">
                                {{\Illuminate\Support\Facades\Session::get('success')}}
                            </div>
                        @endif
                        <div class="card-header">
                            <h4 class="card-title">Maqolalar soni jadvali</h4>
                        </div>
                        <div class="table-responsive">
                            @if(session()->has('messageM'))
                                <div class="alert alert-danger">
                                    {{ session()->get('messageM') }}
                                </div>
                            @endif
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>
                                        №
                                    </th>
                                    <th>
                                        Nashr soni
                                    </th>
                                    <th>
                                        Boshlanish vaqti
                                    </th>
                                    <th>
                                        Tugash vaqti
                                    </th>
                                    <th>
                                        Chiqish vaqti
                                    </th>

                                    <th>
                                        Yili
                                    </th>

                                    <th>
                                        Tahrirlash
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($maqolas as $maqola)
                                    <tr>

                                        <td>
                                            {{$loop->index+1}}
                                        </td>
                                        <td>
                                            {{$maqola->publication}}
                                        </td>
                                        <td>
                                            {{$maqola->start_date}}
                                        </td>
                                        <td style="text-align: justify">
                                            {{$maqola->end_date}}
                                        </td>
                                        <td>
                                            {{ $maqola->show_date }}
                                        </td>

                                        <td>
                                            {{ $maqola->year }}
                                        </td>
                                        <td class="d-flex align-content-center">
                                            <a href="{{ route('maqolas.edit', $maqola) }}" class="d-inline-block btn btn-primary"
                                               style="margin-right: 10px;"><i class="fa-solid fa-pen-to-square"></i>&nbsp;&nbsp;O'zgartirish</a>
                                            <form class="d-inline-block" action="{{route('maqolas.destroy', $maqola) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="d-flex btn btn-danger" data-toggle="tooltip"
                                                        title='Delete'><i class="far fa-trash-alt"></i>&nbsp;&nbsp;O'chirish
                                                </button>
                                            </form>
                                        </td>


                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {{--<div>{{ ($sciens->count() > 0) ? $sciens->links() : '' }}</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">

        $('.show_confirm').click(function (event) {
            var form = $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Siz maqolaning navbatdagi sonini o\'chirmoqchimisiz?`,

                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
