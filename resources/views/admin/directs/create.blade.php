@extends('admin.layouts.master')
@section('title','Yo\'nalish qo\'shish')
@section('content')


    <div class="page-body">
        <div class="row">
            <div class="col-sm-12 m-auto mt-5">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Jurnallarga yo'nalishlar qo'shib chiqish</h4>
                    </div>
                    <div class="card-body">
            <form class="" action="{{route('direct.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="mb-3">
                    <div class="col-form-label">Jurnalni tanlang</div>
                        <select name="journal_id" class="js-example-basic-single col-sm-12">
                        <option disabled selected>Jurnalni tanlang</option>
                        @foreach($journals as $journal)
                            <option value="{{$journal->id}}">{{$journal->sciensName}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mb-3 form">
                    <label for="exampleInputName1">Yo'nalishlarni qo'shing</label>
                    <div class="row"  style="display: flex; align-items: center">
                        <div class="col-md-11">
                            <input value="" name="journal_directs[]" class="form-control" type="text" aria-label="file example">
                        </div>
                        <div class="col-md-1">
                            <button id="addInput" class="btn btn-square btn-primary" type="button" data-toggle="tooltip" title="" role="button" data-bs-original-title="btn btn-primary"><i class="icofont icofont-ui-add"></i></button>
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Yo'nalishlarni qo'shish</button>
                </div>
            </form>
                    </div>

                </div>
            </div>
        </div>

    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $("#addInput").click(function (e) {
                e.preventDefault();
                $(".form").append(`
                     <div class="mb-3">

                        <div class="row" style="display: flex; align-items: center">
                            <label for="exampleInputName1">Yo'nalishni qo'shing</label>
                            <div class="col-md-11">
                                <input value="" name="journal_directs[]" class="form-control" type="text" aria-label="file example">
                            </div>
                <div class="col-md-1">
                    <button id="remove_item_btn" class="btn btn-square btn-danger" type="button" data-toggle="tooltip" title="" role="button" data-bs-original-title="btn btn-primary"><i class="icofont icofont-ui-delete"></i></button>
                </div>
            </div>
        </div>
`);
            });
            $(document).on('click', '#remove_item_btn', function (e) {
                e.preventDefault();
                let row_item = $(this).parent().parent();
                $(row_item).remove();
            });
        });
    </script>

@endpush
