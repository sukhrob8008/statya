<?php

namespace App\Providers;

use App\View\MaqolaSoniComposer;
use Illuminate\Support\ServiceProvider;

class MaqolaSoniProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('main.layouts.master', MaqolaSoniComposer::class);
    }
}
