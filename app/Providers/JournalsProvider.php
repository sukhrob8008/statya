<?php

namespace App\Providers;

use App\View\JournalsComposer;
use Illuminate\Support\ServiceProvider;

class JournalsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('main.layouts.master', JournalsComposer::class);
    }
}
