<?php
/**
 * @var \App\Domain\Articles\Models\Article $model
 */

if (!$model->payed) {
    if (is_int($amount)) {
        return $amount == ($model->price * 100);
    }
    return $amount == $model->price;
}

return false;
