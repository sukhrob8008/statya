<?php
/**
 * @var \App\Domain\Articles\Models\Article $model
 * @var \Goodoneuz\PayUz\Models\Transaction $transaction
 */
$model = \App\Domain\Articles\Models\Article::findOrFail($transaction->transactionable_id);

$model->update([
   'payed' => true,
   'transaction_id' => $transaction->id
]);
