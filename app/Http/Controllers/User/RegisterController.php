<?php

namespace App\Http\Controllers\Auth;

use App\Domain\Articles\Models\Article;
use App\Domain\Articles\Repositories\ArticleRepositories;
use App\Domain\Rules\Repositories\RulesRepositories;
use App\Http\Controllers\Controller;
use App\Models\RegisterModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    function login()
    {
        return view('auth.login');
    }
    function register()
    {
        return view('auth.register');
    }
    function save(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'lname'=>'required',
            'email'=>'required|email',
            'password'=>'required|min:5|max:12',
            'password_confirm'=>'required|min:5|max:12'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->role = 0;
        $pass1 = $request->password;
        $pass2 = $request->password_confirm;
        if ($pass1==$pass2)
        {
            $user->password = Hash::make($request->password);
            $save = $user->save();

            if ($save){
                return back()->with('success','Siz ro\'yxatdan muvofaqqiyatli o\'tdingiz!');
            }else{
                return back()->with('fail','Ro\'yxatdan o\'tishda xatolik!');
            }
        }else{
            return back()->with('fail','Parollar mos emas! Iltimos parollarni tekshirib qayta kiriting');
        }

    }

    function check(Request $request)
    {
//        dd("salom");
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);

        $userInfo = User::where('email','=',$request->email)->first();
        if (!$userInfo)
        {
            return back()->with('fail','Siz pochta manzilingizni xato kiritdingiz! Tekshirib qayta kiriting');
        }else{
            if (Hash::check($request->password, $userInfo->password))
            {

                $data = collect([$userInfo->id,$userInfo->name,$userInfo->lname,$userInfo->role]);
//                Session::put('logged', $data);

                if ($userInfo->role == 1)
                {
                    return redirect()->route('auth.dashboard');
                }elseif ($userInfo->role == 0)
                {
                    return redirect()->route('auth.userboard');
                }

            }else{
                return back()->with('fail','Siz parolingizni xato kiritdingiz! Tekshirib qayta kiriting');
            }
        }
    }

    function dashboard()
    {
//        if (Session::get('logged')[3]==1)
//        {
            $count_notifications = Article::where('status','=','0')->count();
            $all_notifications = Article::where('status','=','0')->get();
            return view('admin.layouts.master', compact('count_notifications','all_notifications'));
//        }else{
//            return redirect()->back();
//        }

    }
    function userboard(RulesRepositories $repositories)
    {

//        if (Session::get('logged')[3]==0)
//        {
            $rules = $repositories->getAll();
            return view('users.layouts.index', compact('rules'));
//        }else{
//            return redirect()->back();
//        }

    }

    function setting()
    {
//        if (Session::get('logged')[3]==0) {
            $user_id = Session::get('logged')[0];
            $userProfil = User::where('id', '=', $user_id)->first();
            return view('users.profil.edit', compact('userProfil'));
//        }else{
//            return redirect()->back();
//        }
    }
    function userEdit(Request $request)
    {
//        if (Session::get('logged')[3]==0) {
            $request->validate([
                'name'=>'required',
                'lname'=>'required',
                'email'=>'required|email',
                'old_pass'=>'required|min:5|max:12',
                'new_pass'=>'required|min:5|max:12',
                'new_pass_con'=>'required|min:5|max:12'
            ]);

            $user_id = Session::get('logged')[0];
            $userProfil = User::where('id','=',$user_id)->first();
            //        dd($userProfil);
            if (Hash::check($request->old_pass, $userProfil->password)){
                if ($request->new_pass == $request->new_pass_con)
                {

                    $userProfil->update([
                        'name'=>$request->name,
                        'lname'=>$request->lname,
                        'email'=>$request->email,
                        'password'=>Hash::make($request->new_pass)
                    ]);
                    $data = collect([$userProfil->id, $userProfil->name, $userProfil->lname]);
                    Session::put('logged', $data);
                    return redirect()->route('auth.userboard');
                }else{
                    return back()->with('fail','Parollar mos emas! Iltimos parollarni tekshirib qayta kiriting');
                }
            }else{
                return back()->with('fail','Eski parolni xato kiritdingiz! Iltimos tekshirib qayta kiriting');
            }


//        }else{
//            return redirect()->back();
//        }

    }


}
