<?php

namespace App\Http\Controllers\User;

use App\Domain\Articles\Actions\ArticleActions;
use App\Domain\Articles\DTO\ArticleDTO;
use App\Domain\Articles\Models\Article;
use App\Domain\Articles\Repositories\ArticleRepositories;
use App\Domain\Articles\Requests\ArticleRequests;
use App\Domain\Direct\Models\Direct;
use App\Domain\Maqola\Models\Maqola;
use App\Domain\Sciens\Repositories\SciensRepositories;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(ArticleRepositories $repositories)
    {
        $articles = Article::where('user_id', '=', Auth::user()->id)->orderBy('id','DESC')->paginate(10);
        return view('users.article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(SciensRepositories $sciensRepositories)
    {

        $sciens = $sciensRepositories->getAll();
        $directs = Direct::all();
        return view('users.article.create', compact('sciens', 'directs'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $maqola = Maqola::latest()->first();

        if ($maqola->start_date <= date('Y-m-d', time()) && $maqola->end_date >= date('Y-m-d')) {

            $articleTitle  = $request->articleTitle;
            $articleScienId =  $request->scien_id;
            $articleDirectId = $request->direct_id;
            $articleAnotation  = $request->articleAnotation;
            $articleUserId  = Auth::user()->id;
            $articleStatus  = 0;
            $articleYear  = date('Y');
            $articlePublication  = $maqola->publication;
            $articleAuthor  = $request->author;
            $articleTime  = date('Y-m-d', time());

            $docFile = $request->file('docFile');
            $extension = $docFile->getClientOriginalExtension();
            $docFileName = time() . Str::random(4);
            $docOrginal = $docFileName . '.' . $extension;
            $docFile->move(public_path('files/'), $docOrginal);

            $pdfFile = $request->file('pdfFile');
            $extensionPdf = $pdfFile->getClientOriginalExtension();
            $pdfFileName = time() . Str::random(4);
            $pdfOrginal = $pdfFileName . '.' . $extensionPdf;
            $pdfFile->move(public_path('files/'), $pdfOrginal);

            $now = new DateTime();
            $array_taqriz = array();
            for($i = 0; $i < count($request->taqriz); $i++)
            {

                $taqrizFile[$i] = $request->taqriz[$i];
                $exceptionTaqriz[$i] = $taqrizFile[$i]->getClientOriginalExtension();
                $taqrizFileName[$i] = time() . Str::random(4);
                $taqrizOrginal[$i] = $taqrizFileName[$i] . '.' . $exceptionTaqriz[$i];
                $taqrizFile[$i]->move(public_path('files/'), $taqrizOrginal[$i]);
                $array_taqriz[$i] = $taqrizOrginal[$i];
            }
//            dd($array_taqriz);
            $string = implode(",",$array_taqriz );
//            dd($string);
            DB::table('articles')->insert([
                'articleTitle' => $articleTitle,
                'docFormat' => $docOrginal,
                'pdfFormat' => $pdfOrginal,
                'taqriz' => $string,
                'scien_id' => $articleScienId,
                'direct_id' => $articleDirectId,
                'articleAnotation' => $articleAnotation,
                'user_id' => $articleUserId,
                'status' => $articleStatus,
                'year' => $articleYear,
                'publication' => $articlePublication,
                'author' => $articleAuthor,
                'time' => $articleTime,
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ]);
        }
        else {
            return redirect()->back()->with('msg', 'Maqolani qabul qilish ' . $maqola->start_date . ' dan ' . $maqola->end_date . " shu vaqt oralig'igacha");
        }

        return redirect()->route('articles.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function notifications()
    {
        $articles = Article::where('status', '=', 0)->paginate(10);
        return view('admin.notifications.notifications', compact('articles'));
    }

    public function indexsuccess()
    {
        $articles = Article::where('status', '=', 1)->orderBy('id','DESC')->paginate(10);
        $maqolas = DB::table('maqolas')->select('show_date')->latest()->first();
        return view('admin.sciens.success', compact('articles', 'maqolas'));
    }

    public function timesuccess()
    {
        $articles = Article::where('status', '=', 1)->orderBy('id','DESC')->paginate(10);
        $maqolas = DB::table('maqolas')->select('show_date','start_date','end_date','publication')->latest()->first();
        return view('admin.sciens.timesuccess', compact('articles', 'maqolas'));
    }

    public function uploadFile(Article $article)
    {
        return view('admin.sciens.uploadfile',compact('article'));
    }

    public function uploadFileStore(Request $request, Article $article)
    {
        $request->validate([
            'file_upload' => ['mimes:doc,docx,pdf','required']
        ]);

        $file = Article::find($article->id);
        if($file->file == null)
        {
            $pdfFile = $request->file('file_upload');
            $extensionPdf = $pdfFile->getClientOriginalExtension();
            $pdfFileName = time() . Str::random(4);
            $pdfOrginal = $pdfFileName . '.' . $extensionPdf;
            $pdfFile->move(public_path('files/'), $pdfOrginal);
            $file->file = $pdfOrginal;
            $file->save();
        }elseif ($file->file){
            $destinationFile = public_path('files/') . $file->file_upload;
            if(\File::exists($destinationFile)){
                \File::delete($destinationFile);
            }
            $pdfFile = $request->file('file_upload');
            $extensionPdf = $pdfFile->getClientOriginalExtension();
            $pdfFileName = time() . Str::random(4);
            $pdfOrginal = $pdfFileName . '.' . $extensionPdf;
            $pdfFile->move(public_path('files/'), $pdfOrginal);
//            dd($pdfOrginal);
            $file->file = $pdfOrginal;
            $file->save();
        }

        return redirect()->route('user.timesuccess');
    }

    public function get_directs(Request $request)
    {
        return response()->json([
            'status'=>true,
           'directs'=>Direct::where('journal_id','=',$request->scien_id)->get()
        ]);
    }

}

