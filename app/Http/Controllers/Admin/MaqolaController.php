<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Articles\Models\Article;
use App\Domain\Maqola\Actions\StoreMaqolaAction;
use App\Domain\Maqola\Actions\UpdateMaqolaAction;
use App\Domain\Maqola\DTO\StoreMaqolaDTO;
use App\Domain\Maqola\DTO\UpdateMaqolaDTO;
use App\Domain\Maqola\Models\Maqola;
use App\Domain\Maqola\Repositories\MaqolaRepository;
use App\Domain\Maqola\Requests\StoreMaqolaRequest;
use App\Domain\Maqola\Requests\UpdateMaqolaRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MaqolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param MaqolaRepository $maqolaRepository
     * @return Factory|View
     */
    public function index(MaqolaRepository $maqolaRepository)
    {
        $maqolas = $maqolaRepository->getAll();
        return view('admin.article.index', ['maqolas' => $maqolas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMaqolaRequest $request
     * @param StoreMaqolaAction $action
     * @return RedirectResponse
     */
    public function store(StoreMaqolaRequest $request, StoreMaqolaAction $action)
    {
        try {
            $dto = StoreMaqolaDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
//            dd($exception);
            return redirect()->back();
        }

        return redirect()->route('maqolas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        $accepts = Article::where('status', '=', 1)
            ->get();
        $date = DB::table('maqolas')->select('start_date', 'end_date','show_date')->get();
        $article = DB::table('articles')->select('time')->get();

        return view('main.pages.show_messenger', ['accepts' => $accepts, 'date' => $date]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Maqola $maqola
     * @return Factory|View
     */
    public function edit(Maqola $maqola)
    {
        return view('admin.article.edit', ['maqola' => $maqola]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMaqolaRequest $request
     * @param Maqola $maqola
     * @param UpdateMaqolaAction $action
     * @return RedirectResponse
     */
    public function update(UpdateMaqolaRequest $request, Maqola $maqola, UpdateMaqolaAction $action)
    {
        try {
            $request->merge([
                'maqola' => $maqola
            ]);
            $dto = UpdateMaqolaDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }

        return redirect()->route('maqolas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Maqola $maqola
     * @return RedirectResponse
     */
    public function destroy(Maqola $maqola)
    {
        $maqola->delete();
        return redirect()->back();
    }
}
