<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Rules\Action\RulesAction;
use App\Domain\Rules\DTO\RulesDTO;
use App\Domain\Rules\Models\Rules;
use App\Domain\Rules\Repositories\RulesRepositories;
use App\Domain\Rules\Requests\RulesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class RulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(RulesRepositories $repositories)
    {

        $rules = $repositories->getAll();
        return view('admin.rules.index', compact('rules'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {

        return view('admin.rules.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RulesRequests $requests, RulesAction $action)
    {

        try {
            $dto = RulesDTO::fromArray($requests->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        return redirect()->route('rules.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * @param Rules $rule
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Rules $rule)
    {

        $rule->delete();
        Alert::success($rule->title, 'Yo\'nalishi o\'chirildi!');
        return redirect()->back();

    }
}
