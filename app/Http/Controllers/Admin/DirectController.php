<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Direct\Actions\StoreDirectAction;
use App\Domain\Direct\DTO\StoreDirectDTO;
use App\Domain\Direct\Models\Direct;
use App\Domain\Direct\Repositories\DirectRepository;
use App\Domain\Direct\Requests\StoreDirectRequest;
use App\Domain\Sciens\Models\Scien;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DirectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function index(DirectRepository $directRepository)
    {
        $directs = $directRepository->getAll();
        return view('admin.directs.index', compact('directs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $journals = Scien::all();

        return view('admin.directs.create', compact('journals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, StoreDirectAction $action)
    {
//        dd($request->journal_directs);
        try {
            $dto = StoreDirectDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        return redirect()->route('direct.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
