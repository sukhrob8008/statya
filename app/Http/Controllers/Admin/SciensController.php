<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Sciens\Actions\SciensActions;
use App\Domain\Sciens\Actions\UpdateSciensActions;
use App\Domain\Sciens\DTO\SciensDTO;
use App\Domain\Sciens\DTO\UpdateSciensDTO;
use App\Domain\Sciens\Models\Scien;
use App\Domain\Sciens\Repositories\SciensRepositories;
use App\Domain\Sciens\Requests\SciensRequests;
use App\Domain\Sciens\Requests\UpdateSciensRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class SciensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(SciensRepositories $repositories)
    {

        $sciens = $repositories->getAll();
        return view('admin.sciens.index', compact('sciens'));


    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {

        return view('admin.sciens.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SciensRequests $requests, SciensActions $actions)
    {

        try {
            $dto = SciensDTO::fromArray($requests->all());
            $actions->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        return redirect()->route('sciens.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Scien $scien
     */
    public function edit(Scien $scien)
    {

        return view('admin.sciens.edit', compact('scien'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateSciensActions $actions, UpdateSciensRequests $requests, Scien $scien)
    {

        try {
            $requests->merge([
                'scien' => $scien
            ]);
            $dto = UpdateSciensDTO::fromArray($requests->all());
            $actions->execute($dto);
        } catch (\Exception $exception) {

            return redirect()->back();
        }

        return redirect()->route('sciens.index')->with('success', 'Siz yo\'nalishni muvofaqqiyatli tahrirladingiz');

    }

    /**
     * @param Scien $scien
     */
    public function destroy(Scien $scien)
    {

        $scien->delete();
        Alert::success($scien->sciensName, 'Yo\'nalishi o\'chirildi!');
        return redirect()->back();

    }
}
