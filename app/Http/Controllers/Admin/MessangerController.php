<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Articles\Models\Article;
use App\Domain\Articles\Repositories\ArticleRepositories;
use App\Domain\Maqola\Models\Maqola;
use App\Domain\Messanger\Actions\MessangerAction;
use App\Domain\Messanger\DTO\MessangerDTO;
use App\Domain\Messanger\Models\Messanger;
use App\Domain\Messanger\Repositories\MessangerRepositories;
use App\Domain\Messanger\Requests\MessangerRequest;
use App\Http\Controllers\Controller;
use App\Models\RegisterModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use RealRashid\SweetAlert\Facades\Alert;

class MessangerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(MessangerRepositories $messangerRepositories)
    {

        $chats = Messanger::where('user_id', '=', Auth::user()->id)->get();
        return view('users.messanger.show', compact('chats'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MessangerRequest $request, MessangerAction $action)
    {

        try {
            $updateStatus = Article::where('id', '=', $request->article_id)->update(['status' => 1]);
            $dto = MessangerDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Tasdiqlangan maqola', 'Siz yuborgan izoh maqola egasi muvofaqqiyatli uzatildi!');
        return redirect()->route('user.notifications');

    }

    /**
     * @param Article $article
     */
    public function show($article)
    {

        $user_id = Article::where('id', '=', $article)->first();

        return view('admin.messanger.create', compact('article', 'user_id'));

    }

    /**
     * @param $article
     */
    public function edit($article)
    {
        $user_id = Article::where('id', '=', $article)->first();
        return view('admin.messanger.disable', compact('article', 'user_id'));

    }

    /**
     * @param MessangerRequest $request
     * @param MessangerAction $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {

    }

    /**
     * @param MessangerRequest $request
     * @param MessangerAction $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(MessangerRequest $request, MessangerAction $action)
    {
        try {
            $updateStatus = Article::where('id', '=', $request->article_id)->update(['status' => -1]);
            $dto = MessangerDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Qaytarilgan maqola', 'Siz yuborgan izoh maqola egasi muvofaqqiyatli uzatildi!');
        return redirect()->route('user.notifications');

    }


    public function show_accept_messenger($publication, $year)
    {

        $accepts = Article::where([
            'publication' => $publication,
            'year' => $year,
            'status' => 1,
        ])
            ->orderBy('id','DESC')
            ->paginate();

        $date = DB::table('maqolas')->select('start_date', 'end_date', 'show_date')->get();


        return view('main.pages.show_messenger', ['accepts' => $accepts, 'date' => $date]);
    }
}
