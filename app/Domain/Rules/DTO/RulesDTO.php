<?php


namespace App\Domain\Rules\DTO;


class RulesDTO
{
    protected string $title;
    protected string $description;

    public static function fromArray(array $data)
    {
        $dto = new RulesDTO();
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description']);
        return $dto;
    }
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }



}
