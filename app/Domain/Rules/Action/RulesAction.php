<?php


namespace App\Domain\Rules\Action;


use App\Domain\Rules\DTO\RulesDTO;
use App\Domain\Rules\Models\Rules;
use Illuminate\Support\Facades\DB;

class RulesAction
{
    public function execute(RulesDTO $rulesDTO)
    {
        DB::beginTransaction();
        try {
            $rules = new Rules();
            $rules->title = $rulesDTO->getTitle();
            $rules->description = $rulesDTO->getDescription();
            $rules->save();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
