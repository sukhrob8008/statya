<?php


namespace App\Domain\Rules\Repositories;


use App\Domain\Rules\Models\Rules;

class RulesRepositories
{
    public function getAll()
    {
        return Rules::paginate(10);
    }
}
