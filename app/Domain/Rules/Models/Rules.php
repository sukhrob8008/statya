<?php

namespace App\Domain\Rules\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rules extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'title',
        'description'
    ];
}
