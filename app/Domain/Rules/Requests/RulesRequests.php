<?php

namespace App\Domain\Rules\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RulesRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>['required'],
            'description'=>['required']
        ];
    }

    public function attributes()
    {
        return [
            'title'=> 'Qoida sarlavhasi',
            'description'=> 'Qoida mazmuni'
        ];
    }
}
