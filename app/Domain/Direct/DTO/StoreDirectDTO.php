<?php

namespace App\Domain\Direct\DTO;

class StoreDirectDTO
{
    private int $journal_id;
    private array $journal_directs;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setJournalId($data['journal_id']);
        $dto->setJournalDirects($data['journal_directs']);
        return $dto;
    }

    /**
     * @return int
     */
    public function getJournalId(): int
    {
        return $this->journal_id;
    }

    /**
     * @param int $journal_id
     */
    public function setJournalId(int $journal_id): void
    {
        $this->journal_id = $journal_id;
    }

    /**
     * @return array
     */
    public function getJournalDirects(): array
    {
        return $this->journal_directs;
    }

    /**
     * @param array $journal_directs
     */
    public function setJournalDirects(array $journal_directs): void
    {
        $this->journal_directs = $journal_directs;
    }


}
