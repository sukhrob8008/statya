<?php

namespace App\Domain\Direct\Actions;

use App\Domain\Direct\DTO\StoreDirectDTO;
use App\Domain\Direct\Models\Direct;
use Illuminate\Support\Facades\DB;

class StoreDirectAction
{
    public function execute(StoreDirectDTO $storeDirectDTO)
    {
        DB::beginTransaction();
        try {
            $count = $storeDirectDTO->getJournalDirects();
            for ($i=0; $i<count($count);$i++)
            {
                $direct = new Direct();
                $direct->journal_id = $storeDirectDTO->getJournalId();
                $direct->journal_directs = $count[$i];
                $direct->save();
            }
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $direct;
    }
}
