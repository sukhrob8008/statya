<?php

namespace App\Domain\Direct\Models;

use App\Domain\Sciens\Models\Scien;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Direct extends Model
{
    use HasFactory;

    public function journal_name()
    {
        return $this->belongsTo(Scien::class, 'journal_id','id');
    }
}
