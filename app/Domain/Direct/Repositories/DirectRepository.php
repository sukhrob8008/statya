<?php

namespace App\Domain\Direct\Repositories;

use App\Domain\Direct\Models\Direct;

class DirectRepository
{
    public function getAll()
    {
        return Direct::all();
    }
}
