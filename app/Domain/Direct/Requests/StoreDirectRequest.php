<?php

namespace App\Domain\Direct\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDirectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'journal_id'=>'required',
            'journal_direct'=>'required'
        ];
    }
}
