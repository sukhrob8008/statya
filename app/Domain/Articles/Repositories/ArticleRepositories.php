<?php


namespace App\Domain\Articles\Repositories;


use App\Domain\Articles\Models\Article;

class ArticleRepositories
{
    public function getAll(){
        return Article::all();
    }
}
