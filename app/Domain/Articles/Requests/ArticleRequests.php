<?php

namespace App\Domain\Articles\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'articleTitle' => ['required', 'string'],
            'scien_id' => ['required'],
            'articleAnotation' => ['required', 'string'],
            'docFile' => ['required', 'mimes:doc,docx'],
            'pdfFile' => ['required', 'mimes:pdf'],
            'taqriz' => ['required',array(), 'mimes:doc,docx'],
            'author' => ['nullable', 'string']
        ];
    }

    public function attributes()
    {
        return [
            'articleTitle' => 'Maqola mavzusi',
            'scien_id' => "Yo'nalish",
            'articleAnotation' => 'Maqola haqida malumot',
            'docFile' => 'Doc fayl',
            'pdfFile' => 'Pdf fayl',
            'taqriz' => 'Taqriz',
            'author' => 'Muallif'
        ];
    }
}
