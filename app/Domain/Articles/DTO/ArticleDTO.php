<?php


namespace App\Domain\Articles\DTO;


use Illuminate\Http\UploadedFile;

class ArticleDTO
{
    /**
     * @var UploadedFile
     */
    private UploadedFile $docFile;


    /**
     * @var UploadedFile
     */
    private UploadedFile $pdfFile;

    /**
     * @var UploadedFile
     */
    private UploadedFile  $taqriz=[];


    private string $articleTitle;
    /**
     * @var int
     */
    private int $scien_id;

    /**
     * @var int
     */
    private int $direct_id;

    /**
     * @var string
     */
    private string $articleAnotation;

    /**
     * @var string|null
     */
    private ?string $author = null;


    public static function fromArray(array $data)
    {
        $dto = new ArticleDTO();
        $dto->setArticleTitle($data['articleTitle']);
        $dto->setScienId($data['scien_id']);
        $dto->setDirectId($data['direct_id']);
        $dto->setArticleAnotation($data['articleAnotation']);
        $dto->setDocFile($data['docFile']);
        $dto->setPdfFile($data['pdfFile']);
        $dto->setTaqriz($data['taqriz']);
        $dto->setAuthor($data['author']);

//        dd($dto);
        return $dto;
    }


    /**
     * @return string
     */
    public function getArticleTitle()
    {
        return $this->articleTitle;
    }

    /**
     * @param string $articleTitle
     */
    public function setArticleTitle($articleTitle)
    {
        $this->articleTitle = $articleTitle;
    }

    /**
     * @return int
     */
    public function getScienId()
    {
        return $this->scien_id;
    }

    /**
     * @param int $scien_id
     */
    public function setScienId($scien_id)
    {
        $this->scien_id = $scien_id;
    }

    /**
     * @return string
     */
    public function getArticleAnotation()
    {
        return $this->articleAnotation;
    }

    /**
     * @param string $articleAnotation
     */
    public function setArticleAnotation($articleAnotation)
    {
        $this->articleAnotation = $articleAnotation;
    }

    /**
     * @return UploadedFile
     */
    public function getDocFile()
    {
        return $this->docFile;
    }

    /**
     * @param UploadedFile $docFile
     */
    public function setDocFile($docFile)
    {
        $this->docFile = $docFile;
    }


    /**
     * @return UploadedFile
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }

    /**
     * @param UploadedFile $pdfFile
     */
    public function setPdfFile($pdfFile)
    {
        $this->pdfFile = $pdfFile;
    }

    /**
     * @return UploadedFile
     */
    public function getTaqriz()
    {
        return $this->taqriz;
    }

    /**
     * @param UploadedFile $taqriz
     */
    public function setTaqriz($taqriz)
    {
        $this->taqriz = $taqriz;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     */
    public function setAuthor(?string $author): void
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getDirectId(): int
    {
        return $this->direct_id;
    }

    /**
     * @param int $direct_id
     */
    public function setDirectId(int $direct_id): void
    {
        $this->direct_id = $direct_id;
    }


}
