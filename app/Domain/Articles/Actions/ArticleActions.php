<?php


namespace App\Domain\Articles\Actions;


use App\Domain\Articles\DTO\ArticleDTO;
use App\Domain\Articles\Models\Article;
use App\Domain\Maqola\Models\Maqola;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


class ArticleActions
{
    public function execute(ArticleDTO $articleDTO)
    {

        DB::beginTransaction();
        try {
            $maqola = Maqola::latest()->first();
//            if ($maqola->start_date <= date('Y-m-d', time()) && $maqola->end_date >= date('Y-m-d')) {
                $article = new Article();
                $article->articleTitle = $articleDTO->getArticleTitle();
                $article->scien_id = $articleDTO->getScienId();
                $article->direct_id = $articleDTO->getDirectId();
                $article->articleAnotation = $articleDTO->getArticleAnotation();
                $article->user_id = Auth::user()->id;
                $article->status = 0;
                $article->year = date('Y');
                $article->publication = $maqola->publication;
                $article->author = $articleDTO->getAuthor();
                $article->time = date('Y-m-d', time());

                $docFile = $articleDTO->getDocFile();
                $extension = $docFile->getClientOriginalExtension();
                $docFileName = time() . Str::random(4);
                $docOrginal = $docFileName . '.' . $extension;
                $docFile->move(public_path('files/'), $docOrginal);
                $article->docFormat = $docOrginal;

                $pdfFile = $articleDTO->getPdfFile();
                $extensionPdf = $pdfFile->getClientOriginalExtension();
                $pdfFileName = time() . Str::random(4);
                $pdfOrginal = $pdfFileName . '.' . $extensionPdf;
                $pdfFile->move(public_path('files/'), $pdfOrginal);
                $article->pdfFormat = $pdfOrginal;

                $taqrizFile = $articleDTO->getTaqriz();
                $exceptionTaqriz = $taqrizFile->getClientOriginalExtension();
                $taqrizFileName = time() . Str::random(4);
                $taqrizOrginal = $taqrizFileName . '.' . $exceptionTaqriz;
                $taqrizFile->move(public_path('files/'), $taqrizOrginal);
                $article->taqriz = $taqrizOrginal;

                $article->save();
//            } else {
//                return redirect()->back()->withErrors('msg','Maqolani qabul qilish vaqti '.$maqola->start_date.' '.$maqola->end_date.' shu oraliqda');
//            }

        } catch (\Exception $exception) {
            DB::rollBack();
//            dd($exception);
            throw $exception;
        }
        DB::commit();
    }
}
