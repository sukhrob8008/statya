<?php

namespace App\Domain\Articles\Models;

use App\Domain\Messanger\Models\Messanger;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $perPage = 3;

    protected $fillable = [
        'id',
        'articleTitle',
        'scien_id',
        'articleAnotation',
        'docFormat',
        'pdfFormat',
        'taqriz',
        'user_id',
        'status',
    ];
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    public function messenger()
    {
        return $this->belongsTo(Messanger::class, 'id','article_id');
    }
}

