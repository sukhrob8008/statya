<?php


namespace App\Domain\Sciens\DTO;


class SciensDTO
{
    protected string $sciensName;

    public static function fromArray(array $data)
    {
        $dto = new SciensDTO();
        $dto->setSciensName($data['sciensName']);
        return $dto;
    }
    /**
     * @return string
     */
    public function getSciensName()
    {
        return $this->sciensName;
    }

    /**
     * @param string $sciensName
     */
    public function setSciensName($sciensName)
    {
        $this->sciensName = $sciensName;
    }


}
