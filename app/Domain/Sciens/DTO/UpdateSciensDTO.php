<?php


namespace App\Domain\Sciens\DTO;


use App\Domain\Sciens\Models\Scien;

class UpdateSciensDTO
{
    protected string $sciensName;

    protected Scien $scien;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setScien($data['scien']);
        $dto->setSciensName($data['sciensName']);
//        $dto->setSciens($data['scien']);
//        $dto->setSciensName($data['sciensName']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getSciensName()
    {
        return $this->sciensName;
    }

    /**
     * @param string $sciensName
     */
    public function setSciensName($sciensName)
    {
        $this->sciensName = $sciensName;
    }

    /**
     * @return Scien
     */
    public function getScien()
    {
        return $this->scien;
    }

    /**
     * @param Scien $scien
     */
    public function setScien($scien)
    {
        $this->scien = $scien;
    }



}
