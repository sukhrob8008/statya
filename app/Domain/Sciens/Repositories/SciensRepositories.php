<?php


namespace App\Domain\Sciens\Repositories;


use App\Domain\Sciens\Models\Scien;

class SciensRepositories
{
    public function getAll()
    {
        return Scien::all();
    }
}
