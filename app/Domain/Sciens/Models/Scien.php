<?php

namespace App\Domain\Sciens\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scien extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'sciensName'
    ];

}
