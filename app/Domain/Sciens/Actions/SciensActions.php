<?php


namespace App\Domain\Sciens\Actions;


use App\Domain\Sciens\DTO\SciensDTO;
use App\Domain\Sciens\Models\Scien;
use Illuminate\Support\Facades\DB;

class SciensActions
{
    public function execute(SciensDTO $sciensDTO)
    {
        DB::beginTransaction();
        try {
            $scien = new Scien();
            $scien->sciensName = $sciensDTO->getSciensName();
            $scien->save();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
