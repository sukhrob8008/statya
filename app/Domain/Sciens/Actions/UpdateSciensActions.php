<?php


namespace App\Domain\Sciens\Actions;


use App\Domain\Sciens\DTO\UpdateSciensDTO;
use Illuminate\Support\Facades\DB;

class UpdateSciensActions
{
    public function execute(UpdateSciensDTO $updateSciensDTO)
    {
        DB::beginTransaction();
        try {
            $scien = $updateSciensDTO->getScien();
            $scien->sciensName = $updateSciensDTO->getSciensName();
            $scien->update();
        }catch (\Exception $exception)
        {
            dd($exception);
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
