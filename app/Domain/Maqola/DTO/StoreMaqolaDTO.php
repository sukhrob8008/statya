<?php


namespace App\Domain\Maqola\DTO;


class StoreMaqolaDTO
{
    private string $start_date;

    private string $end_date;

    private string $show_date;

    private string $year;

    private int $publication;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setStartDate($data['start_date']);
        $dto->setEndDate($data['end_date']);
        $dto->setShowDate($data['show_date']);
        $dto->setYear($data['year']);
        $dto->setPublication($data['publication']);

        return $dto;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param string $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param string $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return string
     */
    public function getShowDate()
    {
        return $this->show_date;
    }

    /**
     * @param string $show_date
     */
    public function setShowDate($show_date)
    {
        $this->show_date = $show_date;
    }

    /**
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param string $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return int
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param int $publication
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    }
}
