<?php


namespace App\Domain\Maqola\DTO;


use App\Domain\Maqola\Models\Maqola;

class UpdateMaqolaDTO
{
    private string $start_date;

    private string $end_date;

    private string $show_date;

    private Maqola $maqola;

    private int $publication;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setStartDate($data['start_date']);
        $dto->setEndDate($data['end_date']);
        $dto->setShowDate($data['show_date']);
        $dto->setMaqola($data['maqola']);
        $dto->setPublication($data['publication']);

        return $dto;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param string $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param string $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return string
     */
    public function getShowDate()
    {
        return $this->show_date;
    }

    /**
     * @param string $show_date
     */
    public function setShowDate($show_date)
    {
        $this->show_date = $show_date;
    }


    /**
     * @return Maqola
     */
    public function getMaqola()
    {
        return $this->maqola;
    }

    /**
     * @param Maqola $maqola
     */
    public function setMaqola($maqola)
    {
        $this->maqola = $maqola;
    }

    /**
     * @return int
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * @param int $publication
     */
    public function setPublication($publication)
    {
        $this->publication = $publication;
    }
}
