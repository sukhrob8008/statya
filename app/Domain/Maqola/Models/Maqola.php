<?php

namespace App\Domain\Maqola\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maqola extends Model
{
    use HasFactory;

    protected $perPage = 10;
}
