<?php


namespace App\Domain\Maqola\Actions;


use App\Domain\Maqola\DTO\UpdateMaqolaDTO;
use App\Domain\Maqola\Models\Maqola;
use Illuminate\Support\Facades\DB;

class UpdateMaqolaAction
{
    /**
     * @param UpdateMaqolaDTO $dto
     * @return Maqola
     * @throws \Exception
     */
    public function execute(UpdateMaqolaDTO $dto)
    {
        DB::beginTransaction();
        try {
            $maqola = $dto->getMaqola();
            $maqola->start_date = $dto->getStartDate();
            $maqola->end_date = $dto->getEndDate();
            $maqola->show_date = $dto->getShowDate();
            $maqola->publication = $dto->getPublication();
            $maqola->save();
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $maqola;
    }
}
