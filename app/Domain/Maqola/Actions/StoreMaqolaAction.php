<?php


namespace App\Domain\Maqola\Actions;


use App\Domain\Maqola\DTO\StoreMaqolaDTO;
use App\Domain\Maqola\Models\Maqola;
use Illuminate\Support\Facades\DB;

class StoreMaqolaAction
{
    /**
     * @param StoreMaqolaDTO $dto
     * @return Maqola
     * @throws \Exception
     */
    public function execute(StoreMaqolaDTO $dto)
    {
        DB::beginTransaction();
        try {
            $publication = DB::table('maqolas')
                ->select('publication')
                ->get();

            $maqolasPublication = DB::table('maqolas')
                ->select('publication')
                ->where('publication', '=', $dto->getPublication())
                ->get();

            $maqolasStartDate = DB::table('maqolas')
                ->select('start_date')
                ->where('start_date', '=', $dto->getStartDate())
                ->get();

            $maqolasEndDate = DB::table('maqolas')
                ->select('end_date')
                ->where('end_date', '=', $dto->getEndDate())
                ->get();

            $maqolasPublicationCount = $maqolasPublication->count();
            $maqolasStartDateCount = $maqolasStartDate->count();
            $maqolasEndDateCount = $maqolasEndDate->count();


            if ($maqolasPublicationCount == 0 && $maqolasStartDateCount == 0 && $maqolasEndDateCount == 0) {
                $maqola = new Maqola();
                $maqola->start_date = $dto->getStartDate();
                $maqola->end_date = $dto->getEndDate();
                $maqola->show_date = $dto->getShowDate();
                $maqola->year = $dto->getYear();
                $maqola->publication = $dto->getPublication();
                $maqola->save();
            } else {
                return redirect()->back()->with('messageM', "Bu sondagi maqola allaqachon qo'shilgan iltimos tekshirib qaytadan kiriting!");
            }
        } catch (\Exception $exception) {
            DB::rollBack();
//            dd($exception);
            throw $exception;
        }
        DB::commit();
        return $maqola;
    }
}
