<?php


namespace App\Domain\Maqola\Repositories;


use App\Domain\Maqola\Models\Maqola;

class MaqolaRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return Maqola::orderBy('id','DESC')->paginate();
    }
}
