<?php

namespace App\Domain\Maqola\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMaqolaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'publication' => ['required','integer'],
            'start_date' => ['required','date'],
            'end_date' => ['required','date'],
            'show_date' => ['required','date'],
        ];
    }

    public function attributes()
    {
        return [
            'publication' => 'Maqola soni',
            'start_date' => 'Qabul boshlanish vaqti',
            'end_date' => 'Qabul tugash vaqti',
            'show_date' => 'Maqola chiqish vaqti',
        ];
    }
}
