<?php


namespace App\Domain\Messanger\Actions;


use App\Domain\Messanger\DTO\MessangerDTO;
use App\Domain\Messanger\Models\Messanger;
use Illuminate\Support\Facades\DB;

class MessangerAction
{
    public function execute(MessangerDTO $messangerDTO)
    {
        DB::beginTransaction();
        try {
            $messanger = new Messanger();
            $messanger->user_id = $messangerDTO->getUserId();
            $messanger->article_id = $messangerDTO->getArticleId();
            $messanger->messanger = $messangerDTO->getMessanger();
            $messanger->save();
        }catch (\Exception $exception)
        {
//            dd($exception);
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
    }
}
