<?php

namespace App\Domain\Messanger\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessangerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'messanger'=>['required','string']
        ];
    }

    public function attributes()
    {
        return [
            'messanger' => 'Qabul qilish',
        ];
    }
}
