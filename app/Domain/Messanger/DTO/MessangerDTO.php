<?php


namespace App\Domain\Messanger\DTO;


class MessangerDTO
{
    private int $user_id;
    private int $article_id;
    private string $messanger;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setMessanger($data['messanger']);
        $dto->setUserId($data['user_id']);
        $dto->setArticleId($data['article_id']);
        return $dto;
    }
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getArticleId()
    {
        return $this->article_id;
    }

    /**
     * @param int $article_id
     */
    public function setArticleId($article_id)
    {
        $this->article_id = $article_id;
    }

    /**
     * @return string
     */
    public function getMessanger()
    {
        return $this->messanger;
    }

    /**
     * @param string $messanger
     */
    public function setMessanger($messanger)
    {
        $this->messanger = $messanger;
    }



}
