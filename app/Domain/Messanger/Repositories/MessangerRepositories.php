<?php


namespace App\Domain\Messanger\Repositories;


use App\Domain\Messanger\Models\Messanger;

class MessangerRepositories
{
    public function getAll()
    {
        return Messanger::all();
    }
}
