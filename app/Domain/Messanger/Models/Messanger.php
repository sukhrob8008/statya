<?php

namespace App\Domain\Messanger\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messanger extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'user_id',
        'article_id',
        'messanger'
    ];
}
