<?php


namespace App\View;


use App\Domain\Maqola\Models\Maqola;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class MaqolaSoniComposer
{
    public function compose(View $view)
    {
        $article = Maqola::latest()->first();
//        dd($article);
        $maqolaaas =  DB::table('maqolas')->select('year')->distinct()->get();
        $maqolas = DB::table('maqolas')
            ->select('publication','id','show_date','year')
            ->where('year','<=', date('Y'))
            ->get();

        $view->with(['article' => $article,'maqolas' => $maqolas, 'maqolaaas' => $maqolaaas]);
    }
}
