<?php

namespace App\View;

use App\Domain\Sciens\Repositories\SciensRepositories;
use Illuminate\View\View;

class JournalsComposer
{
    public function compose(View $view)
    {
        $journals = new SciensRepositories();
        $view->with([
            'journals'=>$journals->getAll()
        ]);
    }
}
