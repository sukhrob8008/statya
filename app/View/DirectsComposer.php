<?php

namespace App\View;

use App\Domain\Direct\Repositories\DirectRepository;
use Illuminate\View\View;

class DirectsComposer
{
    public function compose(View $view)
    {
        $directs = new DirectRepository();
        $view->with([
            'directs'=>$directs->getAll()
        ]);
    }
}
