var DropzoneExample = function () {

    var DropzoneDemos = function () {
        // single files upload
        Dropzone.options.singleFileUpload = {
            paramName: "file", // The name that will be used to transfer the files
            maxFiles: 1,
            maxFilesize: 5, // MB
            accept: function(file, done) {
                if (file.name == "unive.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        };

        // multi files upload
        Dropzone.options.multiFileUpload = {
            paramName: "file", // The name that will be used to transfer the files
            maxFiles: 10,
            maxFilesize: 10, // MB
            accept: function(file, done) {
                if (file.name == "unive.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        };

        // files type validation
        Dropzone.options.fileTypeValidation = {
            paramName: "file", // The name that will be used to transfer the files
            maxFiles: 10,
            maxFilesize: 10, // MB
            acceptedFiles: "image/*,application/pdf,.psd",
            accept: function(file, done) {
                if (file.name == "unive.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        };


    }

    return {
        init: function() {
            DropzoneDemos();
        }
    };
}();

DropzoneExample.init();
