<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->bigInteger('scien_id')->unsigned()->index();
            $table->string('time');
            $table->longText('articleTitle');
            $table->longText('articleAnotation');
            $table->string('docFormat');
            $table->string('pdfFormat');
            $table->string('taqriz');
            $table->bigInteger('status');
            $table->bigInteger('publication');
            $table->boolean('payed')->default(false);
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
