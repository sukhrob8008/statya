<?php

use App\Http\Controllers\Admin\MessangerController;
use App\Http\Controllers\Admin\RulesController;
use App\Http\Controllers\Admin\SciensController;

use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Auth\HelloController::class,'mainboard'])->name('main');

Route::get('/widget', function () {
    return view('main.pages.widget');
})->name('main.widget');

Route::get('/show_messenger/{publication}/{year}', [MessangerController::class, 'show_accept_messenger'])->name('show_messenger');

Route::resource('/messanger', MessangerController::class);


Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin', 'preventBack']], function () {
    Route::get('/', function () {
        return view('admin.layouts.index');
    })->name("admin.dashboard");
    Route::resource('/sciens', SciensController::class);
    Route::resource('/rules', RulesController::class);
    Route::get('/notifications', [UserController::class, 'notifications'])->name('user.notifications');
    Route::get('/success', [UserController::class, 'indexsuccess'])->name('user.success');
    Route::get('/timesuccess', [UserController::class, 'timesuccess'])->name('user.timesuccess');
    Route::get('/uploadfile/{article}', [UserController::class, 'uploadFile'])->name('user.uploadfile');
    Route::put('/uploadfilestore/{article}', [UserController::class, 'uploadFileStore'])->name('user.uploadfilestore');
    Route::resource('/maqolas', \App\Http\Controllers\Admin\MaqolaController::class);
    Route::resource('/direct', \App\Http\Controllers\Admin\DirectController::class);
});

Route::group(['prefix' => 'user', 'middleware' => ['auth', 'isUser', 'preventBack']], function () {
    Route::get('/',[\App\Http\Controllers\Auth\HelloController::class, 'userboard'])->name("user.dashboard")->middleware('verified');
    Route::get('user/setting', [\App\Http\Controllers\Auth\HelloController::class, 'setting'])->name('user.setting');
    Route::put('user/edit', [\App\Http\Controllers\Auth\HelloController::class, 'userEdit'])->name('user.edit');
    Route::get('/subscription', function () {
        return view('users.profil.subscription');
    })->name('user.subscription');
    Route::resource('/articles', UserController::class);
    Route::post('/journal/section', [UserController::class, 'get_directs'])->name('ajax.journal');

});

Route::middleware(['middleware' => 'preventBack'])->group(function () {
    Auth::routes([
        'verify'=>true
    ]);
});
Route::any('/handle/{paysys}',function($paysys){
    (new Goodoneuz\PayUz\PayUz)->driver($paysys)->handle();
});
